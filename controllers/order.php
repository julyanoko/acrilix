<?php

class Order extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->view->productList = $this->model->productList();
        $this->view->title = 'Carrinho de Compra';
        $this->view->render('order/index');
    }

    function add($id)
    {
        // Existe
        if (isset($_SESSION['carrinho'][$id]))
        {
            $_SESSION['carrinho'][$id] += 1;
        }
        else
        {
            $_SESSION['carrinho'][$id] = 1;
        }
        Msg::msgSuccess('Adicionado com sucesso');
        header('location: '. URL .'order');
    }

    //ALTERAR QUANTIDADE
    // Sinal 1 = mais | 2 = menos
    function edit($sinal, $id)
    {
        if ($sinal == 1)
        {
            $_SESSION['carrinho'][$id]++;
        }
        else if ($sinal == 2)
        {
            $_SESSION['carrinho'][$id]--;
            if ($_SESSION['carrinho'][$id] <= 0)
            {
                unset($_SESSION['carrinho'][$id]);
            }
        }
        header('location: '. URL .'order');
    }

    function delete($id)
    {
        // Existe
        if (isset($_SESSION['carrinho'][$id]))
        {
            unset($_SESSION['carrinho'][$id]);
        }

        Msg::msgSuccess('Excluido com sucesso');
        header('location: '. URL .'order');
    }

    function buy()
    {
	// Salvar no banco os dados da compra
        // Se não estiver logado
        if (!isset($_SESSION['user_logged']))
        {
            $_SESSION['buy'] = true;
            Msg::msgSuccess('Primeiro você deve se logar!');
            header('location: '. URL .'login');
            return;
        }
        else
        {
            // Pagamento
            header('location: '.URL.'payment');
            return;
        }
    }
}
