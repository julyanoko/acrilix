<?php

class ProductDesc extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Produto não encontrado!');
            header('location: '. URL .'listProduct');
            return;
        }
        $product = $this->model->getProduct($id);
        if (!isset($product))
        {
            Msg::msgErro('Produto não encontrado!');
            header('location: '. URL .'listProduct');
            return;
        }

        $this->view->product = $product;
        $this->view->title = 'Descrição do Produto';
        $this->view->render('productDesc/index');
    }
}
