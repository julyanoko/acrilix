<?php

class MyOrders extends Controller
{
    function __construct()
    {
        parent::__construct();
        Auth::checkLogged();

    }

    function index()
    {
        $this->view->orderList = $this->model->orderList();
        $this->view->title = 'Meus Pedidos';
        $this->view->render('myOrders/index');
    }
    function details($id = 0)
    {
        if (isset($id) && $id != 0)
        {
            $this->view->title = 'Detalhes do pedido';
            $this->view->order = $this->model->getOrder($id);
            if ($this->view->user == null)
            {
                $this->view->render('myOrders/details');
                return;
            }
            else
            {
                Msg::msgErro('Pedido não encontrado!');
                header('location: '. URL .'myOrders');
                return;
            }
        }
        else
        {
            Msg::msgErro('Pedido não encontrado!');
            header('location: '. URL .'myOrders');
            return;
        }
    }
}
