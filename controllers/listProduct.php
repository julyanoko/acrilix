<?php

class ListProduct extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->view->prdCategoryList = $this->model->prdCategoryList();
        $this->view->productList = $this->model->productList();
        $this->view->title = 'Produtos';
        $this->view->render('listProduct/index');
    }

    function filter($prd_category_id)
    {
        $this->view->prdCategoryList = $this->model->prdCategoryList();
        $this->view->productList = $this->model->productListByPrdCategory($prd_category_id);
        $this->view->title = 'Produtos';
        $this->view->render('listProduct/index');
    }
}
