<?php

class Payment extends Controller
{
    function __construct()
    {
        Auth::checkLogged();
        parent::__construct();
    }

    function index()
    {
        $this->view->title = 'Finalizar Pagamento';
        $this->view->render('payment/index');
    }

    function JustDoIt()
    {
        // Cria o objeto do pagamento
        $paymentRequest = new PagSeguroPaymentRequest();
                
        $product = $this->model->productList();
        
        // Adiciona os valores na tabela de produtos do pedido
        foreach (Session::get('carrinho') as $id => $qt)
        {
            foreach ($product as $idx_product => $value)
            {
                if ($id == $product[$idx_product]['id'])
                {
                    // id - nome - qt - preço
                    $paymentRequest->addItem($product[$idx_product]['id'], 
                                             $product[$idx_product]['name'],
                                             $qt,
                                             $product[$idx_product]['price']);
                    
                }
            }
        }
        
        // Pro pagseguro deixar de ser uma droga e funcionar
        $name = Session::get('user_logged')['name'];
        $name = preg_replace('/\d/', '', $name);
        $name = preg_replace('/[\n\t\r]/', ' ', $name);
        $name = preg_replace('/\s(?=\s)/', '', $name);
        $name = trim($name);
        $name = explode(' ', $name);

        if(count($name) == 1 ) {
            $name[] = ' dos Santos';
        }
        $name = implode(' ', $name);
        
        // Nome - email - ddd - fone
        $paymentRequest->setSender($name,
                                   Session::get('user_logged')['email'],
                                   '41',
                                   '84299829');

        // Birll
        $paymentRequest->setCurrency("BRL");

        // Tipo de frete
        $paymentRequest->setShippingType(1);
        //$paymentRequest->setShippingCost(20);

        $paymentRequest->setReference($product[$idx_product]['id']);

        $credentials = new PagSeguroAccountCredentials('acrilix2016@hotmail.com',
                                                        'C811B0620EE841DCB846A2B9B28C60F3');

        $url = $paymentRequest->register($credentials);

        $this->model->saveOrder();
        header("location: " . $url);
        return;
    }
}
