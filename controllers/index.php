<?php

class Index extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->view->mainBannerList = $this->model->mainBannerList();
        $this->view->productList = $this->model->productList();
        $this->view->title = 'Acrilix';
        $this->view->renderIndex('index/index');
    }
}
