<?php

class Login extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        Auth::checkAlreadyLogged();
        $this->view->title = 'Login';
        $this->view->render('login/index');
    }

    function run()
    {
        Auth::checkAlreadyLogged();
        $this->model->run();
    }

    public function create()
    {
        Auth::checkAlreadyLogged();
        if (!isset($_POST['email']))
        {
            Msg::msgErro('Erro na criação do usuário!');
            header('location: '. URL .'login');
            return;
        }
        $confirm_password = $_POST['confirm_password'];
        $password = $_POST['password'];

        if ($password === $confirm_password)
        {
            $data = array('email' => $_POST['email'],
                          'name' => $_POST['name'],
                          'password' => $_POST['password']);

            if($this->model->create($data) !== false)
            {
                Msg::msgSuccess('Cadastro Realizado');
            }

            header('location: '. URL .'login');
        }
        else
        {
            Msg::msgErro('As senhas não conferem!');
            header('location: '. URL .'login');
        }
    }

    function logout()
    {
        Session::destroy('user_logged');
        header('location: '.URL.'login');
        exit;
    }

    public function recuperarSenhaAjax(){
      if($_POST){
        $emailUsuario = $_POST['email'];
        $novaSenha = $this->model->esqueciSenha($emailUsuario);
        if(!is_array($novaSenha)){
          __autoload('PHPMailer/PHPMailerAutoLoad');
          $mail = new PHPMailer();
          $mail->isSMTP();
          $mail->CharSet = 'UTF-8';
          $mail->Host = 'smtp.gmail.com';
          $mail->SMTPAuth = true;
          $mail->Username = 'contatoacrilix@gmail.com';
          $mail->Password = 'contato@contato';
          $mail->SMTPSecure = 'tls';
          $mail->Port = 587;

          $mail->setFrom($mail->Username, $mail->Username);
          $mail->addAddress($emailUsuario);

          $mail->isHTML(true);                                  // Set email format to HTML

          $mail->Subject = 'Recuperação de senha Acrilix';
          $mail->Body    = "<p><h2>Olá, " . $emailUsuario . "</h2></p>" . "<p>Redefinimos sua senha para você!</p>" . "<p>Login: " . $emailUsuario . "</p>" . "<p>Senha: " . $novaSenha . "</p>";
          if($mail->send()){
            echo json_encode(array('msg' => 'Um e-mail foi enviado para você recuperar sua conta!'));
          }else{
            echo 'Mailer Error: ' . $mail->ErrorInfo;die;
            echo json_encode(array('msg' => 'Não foi possível enviar o e-mail!'));
          }
        }else{
          echo json_encode($novaSenha);
        }
      }else{
        echo json_encode(array('msg' => 'Não foi possível recuperar a senha!'));
      }
      die;
    }

}
