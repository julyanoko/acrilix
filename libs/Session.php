<?php

class Session
{
    public static function init()
    {
        // O arroba é meio gambiarra, mas funciona ¯\_(ツ)_/¯
        @session_start();
        
        // Essas linhas abaixo garantem mais segurança a sessão do usuário
        ini_set('session.cookie_httponly', 1 );
        ini_set('session.cookie_lifetime', 0); 
        ini_set('session.entropy_file', '/dev/urandom');
        ini_set('session.hash_function', 'sha256');
        ini_set('session.use_only_cookies', 1);
        ini_set('session.hash_bits_per_character', 5);
        ini_set('session.cookie_secure', 1);
    }
    
    public static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
    
    public static function get($key)
    {
        if (isset($_SESSION[$key]))
        {
            return $_SESSION[$key];
        }
    }
    
    public static function destroy($key)
    {
        unset($_SESSION[$key]);
    }
}