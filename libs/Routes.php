<?php

/*
 * Classe responsável por gerenciar as passagens de parâmetros
 * através da url
*/
class Routes
{
    private $_url = null;
    private $_controller = null;

    /*
     * Cria a estrutura de passagem de parametros pela url
    */
    public function init()
    {
        $this->_getUrl();

        // Se for recebido uma url vazia chama a index padrão
        if (empty($this->_url[0]))
        {
            $this->_loadDefaultController();
            return false;
        }

        $this->_loadExistingContoller();
        $this->_callControllerMethod();
    }

    /*
     * Carrega a controller que o usuário escolheu
    */
    private function _loadExistingContoller()
    {
        $file = 'controllers/' . $this->_url[0] . '.php';

        if (file_exists($file))
        {
            require $file;
            $this->_controller = new $this->_url[0];
            $this->_controller->loadModel($this->_url[0]);
        }
        else
        {
            $this->_error();
            return false;
        }
    }

    /*
     * Retorna a URL digitada dividida transformada em um array a cada '/' encontrada. Também retira o ultimo /
     */
    private function _getUrl()
    {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }

    /*
     * Carrega a controller padrão caso tenha ocorrido algum erro na passagem de parametros
    */
    private function _loadDefaultController()
    {
        require 'controllers/index.php';
        $this->_controller = new Index();
        $this->_controller->loadModel('index');
        $this->_controller->index();
    }

    /*
     * Passa os argumentos para o controller
     * Cada / após a controller que foi passado pela url conta como um novo parametro, menos o 1º que é a função
     * url[0] = Controller
     * url[1] = Metodo
     * url[2] = Parametro
     * url[3] = Parametro [...]
     * Exemplo: NomeDaContoller->NomeDaFuncao(Parametro1, parametro2, ...);
    */
    private function _callControllerMethod()
    {
        $lenght = count($this->_url);

        if ($lenght > 1)
        {
            if (!method_exists($this->_controller, $this->_url[1]))
            {
                $this->_error();
            }
        }

        switch ($lenght)
        {
            case 5:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                break;
            case 4:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                break;
            case 3:
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            case 2:
                $this->_controller->{$this->_url[1]}();
                break;
            default:
                $this->_controller->index();
                break;
        }
    }

    /*
     * Chama a tela de erro caso a pagina pesquisada não exista
    */
    function _error()
    {
        require_once 'controllers/error.php';
        $this->_controller = new Error();
        $this->_controller->error404();
        exit();
    }
}
