<?php

class Order_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function productList()
    {
        return $this->db->selectAll('SELECT products.id, products.name as name, products.description as description, products.price, products.path1, '
                                  . 'products.prd_categories_id, prd_categories.name as prd_categories_name '
                                  . 'FROM products, prd_categories '
                                  . 'WHERE products.prd_categories_id = prd_categories.id');
    }
}
