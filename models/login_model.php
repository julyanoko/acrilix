<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        if (!isset($_POST['email']))
        {
            Msg::msgErro('Usuário não cadastrado!');
            header('location: '.URL.'login');
            return;
        }
        $sth = $this->db->prepare("SELECT id, name, email, password FROM users WHERE email = :email AND password = :password");
        $sth->execute(array(
                            ':email' => $_POST['email'],
                            ':password' => Hash::create($_POST['password'])
        ));

        $data = $sth->fetch();

        $count = $sth->rowCount();
        if ($count > 0)
        {
            Msg::msgSuccess('Login efetuado com sucesso!');

            // Se não existir essa variável significa que o usuário está efetuando o login de modo normal,
            // Caso contrário ele está vindo da tela de carrinho e deve ser redirecionado para a tela de finalizar compra
            if((!isset($_SESSION['buy'])) || ($_SESSION['buy'] == false))
            {
                    // Login
                    Session::set('user_logged', $data);
                    header('location: '.URL.'index');
                    return;
            }
            else
            {
                    $_SESSION['buy'] = false;
                    // Pagamento
                    Session::set('user_logged', $data);
                    header('location: '.URL.'payment/JustDoIt');
                    return;
            }
        }
        else
        {
            Msg::msgErro('Usuário não cadastrado!');
            header('location: '.URL.'login');
            return;
        }
    }

    public function create($data)
    {
        $values = array('email' => $data['email'],
                        'name' => $data['name'],
                        'password' => Hash::create($data['password']));
        if ($this->db->insert('users', $values) == false)
        {
            if ($this->db->getError() == '23000')
            {
                Msg::msgErro('Email já cadastrado!');
                header('location: '.URL.'login');
                return false;
            }
            else
            {
                Msg::msgErro('Ocorreu um erro inesperado ao tentar criar o usuário, por favor, aguarde uns instantes até tentar novamente!');
                header('location: '.URL.'login');
                return false;
            }
        }
    }

    public function esqueciSenha($email){
      $existe = 'SELECT * FROM users WHERE email = "' . $email . '" LIMIT 1;';
      $sth = $this->db->prepare("SELECT id, name, email, password FROM users WHERE email = :email LIMIT 1");
      $sth->execute(array(
                          ':email' => $email,
      ));
      $data = $sth->fetch();
      $count = $sth->rowCount();
      if ($count > 0){
        $novaSenha = $this->randomPassword();
        $values = array('id' => $data['id'],
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'password' => Hash::create($novaSenha));
        if($this->db->update('users', $values, "`id` = {$data['id']}")){
          return $novaSenha;
        }else{
          return array('msg' => 'Não foi possível recuperar a senha!');
        }
      }else{
        return array('msg' => 'Usuário não cadastrado');
      }
    }

    private function randomPassword() {
      $t1 = time();
      $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
      $pass = array();
      $alphaLength = strlen($alphabet) - 1;
      for ($i = 0; $i < 8; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }
      $t2 = time();
      return $t1 . implode($pass) . $t2;
  }

}
