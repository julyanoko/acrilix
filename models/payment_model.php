<?php

class Payment_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function saveOrder()
    {
        // Insere os dados do pedido
        $values = array('date' => date("Y-m-d H:i:s"),
                        'users_id' => Session::get('user_logged')['id'],
                        'or_status_id' => STATUS_AGUARDANDO_PGT);
        $this->db->insert('orders', $values);

        // Pega o id do pedido
        $sth = $this->db->prepare("SELECT LAST_INSERT_ID() as id");
        $sth->execute();
        $order_id = $sth->fetch();

        // Verifica se o numero de linhas retornadas foi maior que 0
        $count = $sth->rowCount();
        if ($count > 0)
        {
            // Adiciona os valores na tabela de produtos do pedido
            foreach (Session::get('carrinho') as $id => $qt)
            {
                $values = array('orders_id' => $order_id['id'],
                                'products_id' => $id,
                                'quantity' => $qt);
                $this->db->insert('orders_products', $values);
            }
        }
        Session::destroy('carrinho');
        Session::set('carrinho', array());
        Msg::msgSuccess('Compra efetuada!');
        header('location: '.URL.'myOrders');
        return;
    }
    
    
    public function productList()
    {
        $data = $this->db->selectAll('SELECT products.id, products.name as name, products.prd_categories_id, '
                                  . 'prd_categories.name as prd_categories_name, products.price as price, '
                                  . 'products.quantity as quantity, '
                                  . 'products.path1 as path1, products.path2 as path2, products.path3 as path3, '
                                  . 'products.path4 as path4, products.path5 as path5 '
                                  . 'FROM products, prd_categories '
                                  . 'WHERE products.prd_categories_id = prd_categories.id');     
        
        foreach ($data as $key => $value)
        {
           $price = $value['price'];
           $price = str_replace(",",".",$price);
           $data[$key]['price'] = number_format((float)$price, 2);
        }

        return $data;
    }
    
}
