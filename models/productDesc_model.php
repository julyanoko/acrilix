<?php

class ProductDesc_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function getProduct($id)
    {
        $data = $this->db->select('SELECT products.id, products.name as name, products.quantity as quantity, products.prd_categories_id, '
                                  . 'products.description as description,'
                                  . 'prd_categories.name as prd_categories_name, products.price as price, '
                                  . 'products.path1 as path1, products.path2 as path2, products.path3 as path3, '
                                  . 'products.path4 as path4, products.path5 as path5 '
                                  . 'FROM products, prd_categories '
                                  . 'WHERE products.prd_categories_id = prd_categories.id '
                                  . 'AND products.id = :id', array(':id' => $id));  
        
        $price = $data['price'];
        $price = str_replace(",",".",$price);
        $data['price'] = number_format((float)$price, 2);

        return $data;    
    }
}