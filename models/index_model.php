<?php

class Index_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function productList()
    {
        $data = $this->db->selectAll('SELECT products.id, products.name as name, products.prd_categories_id, '
                                  . 'prd_categories.name as prd_categories_name, products.price as price, '
                                  . 'products.quantity as quantity, '
                                  . 'products.path1 as path1, products.path2 as path2, products.path3 as path3, '
                                  . 'products.path4 as path4, products.path5 as path5 '
                                  . 'FROM products, prd_categories '
                                  . 'WHERE products.prd_categories_id = prd_categories.id');     
        
        foreach ($data as $key => $value)
        {
           $price = $value['price'];
           $price = str_replace(",",".",$price);
           $data[$key]['price'] = number_format((float)$price, 2);
        }

        return $data;
    }
    
    public function mainBannerList()
    {
        return $this->db->selectAll('SELECT id, name, img_url, path '
                                  . 'FROM main_banners'); 
    }
}
