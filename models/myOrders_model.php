<?php

class MyOrders_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function orderList()
    {
        return $this->db->selectAll('SELECT id, date, users_id, or_status_id '
                                    . 'FROM orders WHERE users_id = :id', array(':id' => Session::get('user_logged')['id']));
    }


    public function getOrder($id)
    {
        $sth = $this->db->prepare("SELECT * FROM orders_products, orders, products WHERE orders.id = orders_products.orders_id AND products.id = orders_products.products_id AND orders.id = :id");
        $sth->execute(array(':id' => $id));

        $data = $sth->fetch();

        return $data;
    }
}
