<?php

class Msg
{
    public static function msgErro($msg)
    {
        Session::set('erro', $msg);
    }
    
    public static function msgSuccess($msg)
    {
        Session::set('success', $msg);
    }
}