<?php

class Auth
{
    /*
     * Verifica se o cliente está logado
     * Se não retorna a tela de login de clientes
     */
    public static function checkLogged()
    {
        // Se não estiver logado retorna false
        if (!isset($_SESSION['user_logged']))
        {
            Msg::msgErro('Você deve estar logado para acessar esta área!');
            unset($_SESSION['user_logged']);
            header('location: '.URL.'login');
            exit;
        }
    }

    /*
     * Verifica se o cliente já está logado
     * Se já estiver retorna para a sua tela principal
     */
    public static function checkAlreadyLogged()
    {
        // Se não estiver logado retorna false
        if (isset($_SESSION['user_logged']))
        {
            header('location: '.URL.'index');
            exit;
        }
    }

    /*
     * Verifica se o cliente está logado
     * Se não retorna a tela de login de clientes
     */

}
