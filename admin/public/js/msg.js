function showErrorMsg(msg)
{
    sweetAlert({
        text: msg,
        type: 'error'
    });
}
function showSuccessMsg(msg)
{
    sweetAlert({
        text: msg,
        type: 'success'
    });
}

function showConfirmMsg(url_total, controller_name, item)
{
    swal({
        title: 'Atenção',
        text: 'Tem certeza que deseja excluir este item?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Excluir',
        cancelButtonText: 'Cancelar'
    }).then(function () {
        window.location=url_total+controller_name+'/delete/' + item;
    });
}