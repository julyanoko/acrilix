<?php

class Hash
{
    /*
     * @param string $data O que será criptografado
     * @return string A hash que foi criada
     */
    public static function create($data)
    {
        $context = hash_init('sha512', HASH_HMAC, HASH_KEY);
        
        hash_update($context, $data);
        return hash_final($context);
    }
}