<?php

class View
{
    function __construct()
    {
    }

    /*
     * Renderiza a view com o nome que foi chamado no controller
     * Header e footer padrão
    */
    public function render($name)
    {
        require 'views/header.php'; 
        require 'views/' . $name . '.php';
        require 'views/footer.php';
    }

    /*
     * Renderiza a view com o nome que foi chamado no controller
     * Sem Header e footer
    */
    public function renderWithoutHeader($name)
    {
        require 'views/' . $name . '.php';
    }
}
