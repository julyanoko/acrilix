<?php

class Controller
{
    function __construct()
    {
        // Cria uma nova view que todo controller precisa para
        // Renderiza-la e passar parametros para tela
        $this->view = new View();
    }
    
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path))
        {
            require 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }
}