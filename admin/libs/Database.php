<?php

/*
 * Exemplos de querys sem usar as funcões que estão aqui:
 * 
 * - Select ALL:
 *    $sth = $this->db->prepare('SELECT id, email FROM user');
 *    $sth->execute();
 *    return $sth->fetchAll();
 * 
 * - Select:
 *    $sth = $this->db->prepare('SELECT id, email FROM user where id = :id');
 *    $sth->execute(array(':id' => $id));
 *    return $sth->fetch();
 *        
 * - Insert:
 *    $sth = $this->db->prepare('INSERT INTO user(`email`, `password`) VALUES (:email, :password)');
 *    $sth->execute(array(':email' => $data['email'], 
 *    ':password' => Hash::create($data['password'])));
 * 
 * - Update:
 *    $sth = $this->db->prepare('UPDATE user SET `email` = :email WHERE id = :id');
 *    $sth->execute(array(':id' => $data['id'],
 *    ':email' => $data['email']));
 * 
 * - Delete:
 *    $sth = $this->db->prepare('DELETE FROM user WHERE id = :id');
 *    $sth->execute(array(':id' => $id));
 */

class Database extends PDO
{
    private $_database_error = null;
    
    public function __construct($DB_TYPE, $DB_HOST, $DB_NAME, $DB_USER, $DB_PASS)
    {        
        parent::__construct($DB_TYPE . ':host=' . $DB_HOST . ';dbname='. $DB_NAME, $DB_USER, $DB_PASS);
        parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        //parent::setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
    }
    
    // Seleciona várias linhas
    // Array = Where
    public function selectAll($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        
        if(!$sth->execute())
        {
            $this->_database_error = $sth->errorCode();
        }
        
        return $sth->fetchAll($fetchMode);
    }
    
    //Seleciona uma linha
    // Array = Where
    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);
        foreach ($array as $key => $value) {
            $sth->bindValue("$key", $value);
        }
        
        if(!$sth->execute())
        {
            $this->_database_error = $sth->errorCode();
            return false;
        }
        
        return $sth->fetch($fetchMode);
    }
    
    public function insert($table, $data)
    {
        ksort($data);
        
        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));
        
        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES ($fieldValues)");
        
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        
        if(!$sth->execute())
        {
            $this->_database_error = $sth->errorCode();
            return false;
        }
        return true;
    }
    
    public function update($table, $data, $where)
    {
        ksort($data);
        
        $fieldDetails = NULL;
        foreach($data as $key=> $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');
        
        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        
        if(!$sth->execute())
        {
            $this->_database_error = $sth->errorCode();
            return false;
        }
        return true;
    }
    
    public function delete($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }
    
    public function getError()
    {
        return $this->_database_error;
    }
}