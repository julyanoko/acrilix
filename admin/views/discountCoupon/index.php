<div align="center">
    <h1 >Área Administrativa do cupom de desconto</h1>
    <div>
        <br><br>

        <div class="container" style="padding-top:30px">
            <div align="right">
                <a href="<?php echo URL_ADMIN ?>discountCoupon/add" class="btn btn-primary btn-sm" >Adicionar Cupom</a>
            </div>
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">
                    <thead class="thead-inverse">
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="code" class="col-md-7">Código</th>
                            <th id="percentage" class="col-md-2">Porcentagem</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="code" class="col-md-7">Código</th>
                            <th id="percentage" class="col-md-2">Porcentagem</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </tfoot>   
                    <?php
                    foreach ($this->discountCouponList as $key => $value)
                    {
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id_table"><?php echo $value['id'] ?></td>
                                <td headers="code"><?php echo $value['code'] ?></td>
                                <td headers="percentage"><?php echo $value['percentage'] ?>%</td>
                                <td headers="action"><a class="btn btn-warning btn-xs" href="<?php echo URL_ADMIN; ?>discountCoupon/edit/<?php echo $value['id'] ?>">Editar</a>
                                <a class="btn btn-danger btn-xs" onclick="showConfirmMsg('<?php echo URL_ADMIN; ?>', 'discountCoupon', <?php echo $value['id'] ?>)">Excluir</a>
                                </td>
                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>  
    </div>
</div>    

