<link href="<?php echo URL_ADMIN; ?>public/css/DataTables/datatables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo URL_ADMIN; ?>public/css/DataTables/buttons.dataTables.min.css" rel="stylesheet" type="text/css"/>

<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/buttons.print.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/jszip.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/pdfmake.min.js" type="text/javascript"></script>
<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/vfs_fonts.js" type="text/javascript"></script>

<table id="my_table" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Descrição</th>
            <th>Resultado</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Descrição</th>
            <th>Resultado</th>
        </tr>
    </tfoot>  
    <tbody>
        <tr>
            <td>
                <?php echo $this->listReport['num_vendas']['desc'];?>
            </td>
            <td>
                <?php echo $this->listReport['num_vendas']['val'];?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $this->listReport['mais_vendido']['desc'];?>
            </td>
            <td>
                <?php echo $this->listReport['mais_vendido']['val'];?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $this->listReport['mais_vendido_qt']['desc'];?>
            </td>
            <td>
                <?php echo $this->listReport['mais_vendido_qt']['val'];?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $this->listReport['valor_arrecadado']['desc'];?>
            </td>
            <td>
                R$<?php echo $this->listReport['valor_arrecadado']['val'];?>
            </td>
        </tr>
    </tbody>
</table>


<script src="<?php echo URL_ADMIN; ?>public/js/DataTables/Custom.js" type="text/javascript"></script>
