<div align="center">
    <h1 >Área Administrativa da Categoria</h1>
    <div>
        <br><br>

        <div class="container" style="padding-top:30px">
            <div align="right" >
                <a href="<?php echo URL_ADMIN ?>prdcategory/add" class="btn btn-primary btn-sm" >Adicionar Categoria</a>
            </div>
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">
                    <thead class="thead-inverse">
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="name" class="col-md-9">Nome</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="name" class="col-md-9">Nome</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </tfoot>   
                    <?php
                    foreach ($this->prdCategoryList as $key => $value)
                    {
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id_table"><?php echo $value['id'] ?></td>
                                <td headers="name"><?php echo $value['name'] ?></td>
                                <td headers="action"><a class="btn btn-warning btn-xs" href="<?php echo URL_ADMIN; ?>prdcategory/edit/<?php echo $value['id'] ?>">Editar</a>
                                <a class="btn btn-danger btn-xs" onclick="showConfirmMsg('<?php echo URL_ADMIN; ?>', 'prdcategory', <?php echo $value['id'] ?>)">Excluir</a>
                                </td>
                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>  
    </div>
</div>    

