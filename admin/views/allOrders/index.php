<div align="center">
    <h1>Área Administrativa dos Pedidos</h1>
    <div>
        <br><br>
        <div class="container" style="padding-top:30px">
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">

                    <thead class="thead-inverse">
                        <tr>
                            <th id="id" class="col-md-2">Código</th>
                            <th id="date" class="col-md-5">Data</th>
                            <th id="or_status_id" class="col-md-5">Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th id="id" class="col-md-2">Código</th>
                            <th id="date" class="col-md-5">Data</th>
                            <th id="or_status_id" class="col-md-5">Status</th>
                        </tr>
                    </tfoot>
                    <?php
                    foreach ($this->orderList as $key => $value)
                    {
                      $data = explode("-", $value['date']);
                      $data = $data[2] . "/" . $data[1] . "/" . $data[0];
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id"><?php echo $value['id'] ?></td>
                                <td headers="date"><?php echo $data ?></td>
                                <td headers="or_status_id"><?php
                                    if ($value['or_status_id'] == STATUS_AGUARDANDO_PGT)
                                    {
                                        echo STATUS_AGUARDANDO_PGT_S;
                                    }
                                ?></td>
                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
