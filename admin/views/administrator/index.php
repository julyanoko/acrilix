<div align="center">
    <h1>Área Administrativa do Administrador</h1>
    <div>
        <br><br>

        <div class="container" style="padding-top:30px">
            <div align="right" >
                <a href="<?php echo URL_ADMIN ?>administrator/add" class="btn btn-primary btn-sm" >Adicionar administrador</a>
            </div>
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">
                    <thead class="thead-inverse">
                        <tr>
                            <th class="col-md-1" id="id_table">Id</th>
                            <th class="col-md-4" id="name">Name</th>
                            <th class="col-md-5" id="email">Email</th>
                            <th class="col-md-2" id="action">Ações</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th class="col-md-1" id="id_table">Id</th>
                            <th class="col-md-4" id="name">Name</th>
                            <th class="col-md-5" id="email">Email</th>
                            <th class="col-md-2" id="action">Ações</th>
                        </tr>
                    </tfoot>   
                    <?php
                    $logged_data = Session::get('adm_logged');
                    foreach ($this->administratorList as $key => $value)
                    {
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id_table"><?php echo $value['id'] ?></td>
                                <td headers="name"><?php echo $value['name'] ?></td>
                                <td headers="email"><?php echo $value['email'] ?></td>

                                <td headers="action"><a class="btn btn-warning btn-xs" href="<?php echo URL_ADMIN; ?>administrator/edit/<?php echo $value['id'] ?>">Editar</a>
                                    <?php if ($logged_data['id'] != $value['id']): ?>
                                        <a class="btn btn-danger btn-xs" onclick="showConfirmMsg('<?php echo URL_ADMIN; ?>', 'administrator', <?php echo $value['id'] ?>)">Excluir</a>
                                    <?php else: ?>
                                        <a class="btn btn-danger btn-xs disabled" href="#">Logado</a>
                                    <?php endif ?>
                                </td>
                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>  
    </div>
</div>