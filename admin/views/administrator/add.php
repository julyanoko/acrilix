<form method="post" action="<?php echo URL_ADMIN; ?>administrator/create">
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" >
                                <a><b>Cadastrar Administrador</b></a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nome" name="name" required/>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" autocomplete="off" name="password" id="inputPassword" placeholder="Senha" required>
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" autocomplete="off" name="confirm_password" id="inputPassword" placeholder="Confirmar Senha" required>
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Gravar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>