<p><h2>Como Fazer o site funcionar em outro servidor!</h2></p>
<br> 
- <b>Para que o banco de dados funcione:</b> <br>
Habilitar o PDO no php.ini.
<br>
- <b>Para que seu .htaccess funcione:</b> <br>
Certifique-se que a diretiva <b>AllowOverride</b> esteja nos arquivos de configuração do apache (Windows = httpd.conf). Ela deve ficar dentro de &lt;Directory /&gt; deve ser alterada de <b>AllowOverride None</b> para: <b>AllowOverride All</b>.
<br>Habilitar o módulo <b>mod_deflate</b> no apache. 
<br>Após isso é só reiniciar o apache da maquina e já deve estar funcionando.
<br><br>
- <b>Configurações:</b> <br>
Não esquecer de mudar as configurações do site na pasta config (dentro do projeto), principalmente os paths.
<br>
Mudar <b>allow_url_fopen</b> no php.ini para Off, se não estiver.