<html>
    <head>
        <link rel="stylesheet" href="<?php echo URL_ADMIN; ?>public/css/error.css" charset="UTF-8">
    </head>
    <body>
        <b>
            <span class="neg"><?php echo $this->msg; ?></span>
            <p>
                A fatal exception 0E has occurred at 0028:C001E36 in VXD VMM(01) * <br>
                &nbsp;00010E36. The current application will be terminated.
            </p>
            <p>
                * Press any key to terminate the current application.<br />

                * Press CTRL+ALT+DEL again to restart your computer. You will<br />
                &nbsp; lose unsaved information any unsaved information in all applications.
            </p>
            <a href="<?php echo URL_ADMIN . 
                    (isset($_SESSION['adm_logged']) ?
                    'index' : 
                    '../index')
                    ?>
                    "><center>Click aqui para continuar<span class="blink_text">_</span></center></a>
        </b>
    </body>
</html>