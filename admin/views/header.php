<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="pt-br">
  <head>

    <!-- META TAG'S -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta charset="utf-8">
    <meta name="description" content="Acrilix - Peças em acrilico." />
    <meta name="keywords" content="Acrilix - Peças em acrilico."/>
    <meta name="robots" content="index, follow">
    <meta http-equiv="content-language" content="pt-br"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title (padrão: Acrilix) -->
    <title><?php echo isset($this->title) ? $this->title : 'Acrilix'; ?></title>

    <!-- jquery -->
    <script type='text/javascript' src="<?php echo URL_ADMIN; ?>public/js/jquery.js" charset="UTF-8"></script>

    <!-- Icon da barra de endereços / abas -->
    <link rel="shortcut icon" href="<?php echo URL_ADMIN; ?>public/images/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap -->
    <link href="<?php echo URL_ADMIN;?>public/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <!-- Font Awesome -->
    <link href="<?php echo URL_ADMIN;?>public/css/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <!-- NProgress -->
    <link href="<?php echo URL_ADMIN;?>public/css/Dashboard/nprogress.css" rel="stylesheet" type="text/css"/>
    <!-- jQuery custom content scroller -->
    <link href="<?php echo URL_ADMIN;?>public/css/Dashboard/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>

    <!-- Sweet Alert-->
    <link rel="stylesheet" href="<?php echo URL_ADMIN; ?>public/css/SweetAlert/sweetalert2.css" charset="UTF-8">
    <script type='text/javascript' src="<?php echo URL_ADMIN; ?>public/js/SweetAlert/sweetalert2.min.js" charset="UTF-8"></script>

    <!-- javascript comuns em todas as telas -->
    <script type='text/javascript' src="<?php echo URL_ADMIN; ?>public/js/msg.js" charset="UTF-8"></script>

    <!-- Custom Theme Style -->
    <link href="<?php echo URL_ADMIN;?>public/css/Dashboard/custom.css" rel="stylesheet" type="text/css"/>

    <!-- css comuns em todas as telas -->
    <link rel="stylesheet" href="<?php echo URL_ADMIN; ?>public/css/style.css" charset="UTF-8">

    <!-- Jquery Mask -->
    <script type='text/javascript' src="<?php echo URL_ADMIN;?>public/js/JqueryMask/jquery.mask.js" charset="UTF-8"></script>
  </head>

  <body class="nav-md">
    <?php
        $adm_logged = SESSION::get('adm_logged');
    ?>

    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo URL_ADMIN;?>index" class="site_title"><i class="fa fa-gear"></i> <span>Acrilix</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                    &nbsp;
                    <img src="<?php echo URL_ADMIN;?>../public/images/util/user.jpg" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bem vindo,</span>
                <h2><?php echo $adm_logged['name']?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>&nbsp;</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-archive"></i> Produtos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="<?php echo URL_ADMIN;?>prdcategory/add">Cadastar Categoria</a></li>
                    <li><a href="<?php echo URL_ADMIN;?>prdcategory">Gerenciar Categorias</a></li>
                    <li><a href="<?php echo URL_ADMIN;?>product/add">Cadastrar Produto</a></li>
                    <li><a href="<?php echo URL_ADMIN;?>product">Gerenciar Produtos</a></li>
                </ul>
                    <li><a><i class="fa fa-shopping-cart"></i> Pedidos <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                      <li><a href="<?php echo URL_ADMIN;?>allOrders">Listar Pedidos</a></li>
                    </ul>
                  <li><a><i class="fa fa-users"></i> Usuários<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo URL_ADMIN;?>administrator">Administradores</a></li>
                      <li><a href="<?php echo URL_ADMIN;?>user">Clientes</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-home"></i> Relatórios <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo URL_ADMIN;?>reportSales">Vendas</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-flag"></i> Banners <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo URL_ADMIN;?>mainBanner/add">Cadastar Banner</a></li>
                        <li><a href="<?php echo URL_ADMIN;?>mainBanner">Gerenciar Banner</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-money"></i> Cupons de Desconto <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo URL_ADMIN;?>discountCoupon/add">Cadastar Cupons</a></li>
                        <li><a href="<?php echo URL_ADMIN;?>discountCoupon">Gerenciar Cupons</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-book"></i>Ajuda<span class="fa fa-help"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo URL_ADMIN;?>help">Ajuda</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                &nbsp;&nbsp;
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo URL_ADMIN; ?>login/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Sair</a></li>
              </ul>
            </nav>
          </div>

        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main" style="min-height: 618px">
