        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Copyright &copy; <?= (date('Y') == 2016) ? date('Y') : ("2016" . " - " . date('Y')) ?>. Acrilix. Todos os Direitos Reservados | Desenvolvido por <a href="#">Mbc Sistemas</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

<!-- jQuery -->
<script src="<?php echo URL_ADMIN; ?>public/js/jquery.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="<?php echo URL_ADMIN; ?>public/js/bootstrap.js" type="text/javascript"></script>
<!-- FastClick -->
<script src="<?php echo URL_ADMIN; ?>public/js/Dashboard/fastclick.js" type="text/javascript"></script>
<!-- NProgress -->
<script src="<?php echo URL_ADMIN; ?>public/js/Dashboard/nprogress.js" type="text/javascript"></script>
<!-- jQuery custom content scroller -->
<script src="<?php echo URL_ADMIN; ?>public/js/Dashboard/jquery.mCustomScrollbar.js" type="text/javascript"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo URL_ADMIN; ?>public/js/Dashboard/custom.js" type="text/javascript"></script>
</body>
</html>

<!-- Trecho de código responsável por mandar mensagens para a tela do usuário -->

<?php
if (isset($_SESSION['erro']))
{
    ?>
    <script type='text/javascript' charset="UTF-8" >showErrorMsg(<?php echo json_encode($_SESSION['erro']); ?>);</script>   
    <?php
    Session::destroy('erro');
}
if (isset($_SESSION['success']))
{
    ?>
    <script type='text/javascript' charset="UTF-8" >showSuccessMsg(<?php echo json_encode($_SESSION['success']); ?>);</script>   
    <?php
    Session::destroy('success');
}
