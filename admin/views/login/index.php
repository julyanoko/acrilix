<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang='pt-br'>
    <head>

        <!-- Title (padrão: Acrilix) -->
        <title><?php echo isset($this->title) ? $this->title : 'Autenticação LazyCom'; ?></title>
        <!-- Icon da barra de endereços / abas -->
        <link rel="shortcut icon" href="<?php echo URL; ?>public/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
        <!-- jquery -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/jquery.js" charset="UTF-8"></script>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap.css" charset="UTF-8">
        <link rel="stylesheet" href="<?php echo URL; ?>public/fonts/glyphicons-halflings-regular.svg" charset="UTF-8">
        <script type='text/javascript' src="<?php echo URL; ?>public/js/bootstrap.js" charset="UTF-8"></script>
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/ie10-viewport-bug-workaround.css" charset="UTF-8">
        <!-- Sweet Alert-->
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/SweetAlert/sweetalert2.css" charset="UTF-8">
        <script type='text/javascript' src="<?php echo URL; ?>public/js/SweetAlert/sweetalert2.min.js" charset="UTF-8"></script>
        <!-- Jquery Mask -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/JqueryMask/jquery.mask.min.js" charset="UTF-8"></script>
        <!-- javascript comuns em todas as telas -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/msg.js" charset="UTF-8"></script>
        <script type='text/javascript' src="<?php echo URL; ?>public/js/ie-emulation-modes-warning.js" charset="UTF-8"></script>
        <!-- css comuns em todas as telas -->
        <link rel="stylesheet" href="<?php echo URL_ADMIN; ?>public/css/style.css" charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <?php
        $adm_logged = SESSION::get('adm_logged');
        $username = $adm_logged['name'];
        ?>
        <!-- Navigation -->


<div class="container">
  <div class="wrapper">
    <form class="form-signin" id="login-form" action="<?php echo URL_ADMIN;?>login/run"  method="post" role="form">
      <center><img style="max-width:90%" src="<?php echo URL_ADMIN; ?>public/images/logo-lazycom.png"></a></center></br>

      <input type="email" class="form-control" name="email" id="inputEmail" placeholder="Email" required="" autofocus="" />
      <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Senha" required=""/>
      <label class="checkbox">
    </label>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button></br>
    </form>
  </div>
</div>
