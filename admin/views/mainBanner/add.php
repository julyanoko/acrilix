<div class="container">
    <form method="post" action="<?php echo URL_ADMIN; ?>mainBanner/create" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" >
                                <a><b>Cadastrar Banner</b></a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Descrição do Banner" class="form-control" name="name" required/>
                                </div>
                                <div class="form-group">
                                    <input type="url" placeholder="URL - ex: http://www.google.com.br" class="form-control" name="img_url" required/>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Banner" class="form-control" name="imagem" required></input>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Gravar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>