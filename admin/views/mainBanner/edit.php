<form method="post"  enctype="multipart/form-data" action="<?php echo URL_ADMIN; ?>mainBanner/editSave/<?php echo $this->banner['id']; ?>">
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" >
                                <a><b>Editar Banner</b></a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Descrição do Banner" class="form-control" name="name" value="<?php echo $this->banner['name']; ?>" required/>
                                </div>
                                <div class="form-group">
                                    <input type="url" placeholder="URL - ex: http://www.google.com.br" class="form-control money" name="img_url" value="<?php echo $this->banner['img_url']; ?>" required/>
                                </div>
                                <?php if ($this->banner['path'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->banner['path']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Banner" class="form-control" name="imagem"></input>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Gravar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>