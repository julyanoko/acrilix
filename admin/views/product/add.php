
    <div class="container">
      <form method="post" action="<?php echo URL_ADMIN; ?>product/create" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" >
                                <a><b>Cadastrar Produto</b></a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Nome do Produto" class="form-control" name="name" required/>
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Preço (R$)" class="form-control money" name="price" required/>
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Quantidade em estoque" class="form-control" name="quantity"/>
                                </div>
                                <div class="form-group">
                                    <textarea type="text" style="resize:vertical" placeholder="Descrição" class="form-control" name="description" required></textarea>
                                </div>
                                <div class="form-group">
                                    <select name="prd_categories_id" placeholder="Categoria" class="form-control" required>
                                        <?php
                                        foreach ($this->prdCategoryList as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem Principal" class="form-control" name="imagem1" required></input>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 2" class="form-control" name="imagem2"></input>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 3" class="form-control" name="imagem3"></input>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 4" class="form-control" name="imagem4"></input>
                                </div>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 5" class="form-control" name="imagem5"></input>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Gravar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script src="<?php echo URL_ADMIN;?>public/js/JqueryMask/Masks/price_mask.js" type="text/javascript" charset="UTF-8"></script>
