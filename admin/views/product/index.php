<div align="center">
    <h1>Área Administrativa do Produto</h1>
    <div>
        <br><br>

        <div class="container" style="padding-top:30px">
            <div align="right" >
                <a href="<?php echo URL_ADMIN ?>product/add" class="btn btn-primary btn-sm" >Adicionar Produto</a>
            </div>
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">

                    <thead class="thead-inverse">
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="name" class="col-md-3">Nome</th>
                            <th id="name" class="col-md-2">Quantidade em estoque</th>
                            <th id="price" class="col-md-1">Preço (R$)</th>
                            <th id="prd_categories_id" class="col-md-3">Categoria</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th id="id_table" class="col-md-1">Id</th>
                            <th id="name" class="col-md-3">Nome</th>
                            <th id="name" class="col-md-2">Quantidade em estoque</th>
                            <th id="price" class="col-md-1">Preço (R$)</th>
                            <th id="prd_categories_id" class="col-md-3">Categoria</th>
                            <th id="action" class="col-md-2">Ações</th>
                        </tr>
                    </tfoot>   
                    <?php
                    $logged_data = Session::get('adm_logged');
                    foreach ($this->productList as $key => $value)
                    {
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id_table"><?php echo $value['id'] ?></td>
                                <td headers="name"><?php echo $value['name'] ?></td>
                                <td headers="quantity"><?php echo $value['quantity'] ?></td>
                                <td class="money" headers="price"><?php echo $value['price'] ?></td>
                                <td headers="prd_categories_id"><?php echo $value['prd_categories_name'] ?></td>
                                <td headers="action"><a class="btn btn-warning btn-xs" href="<?php echo URL_ADMIN; ?>product/edit/<?php echo $value['id'] ?>">Editar</a>
                                    <a class="btn btn-danger btn-xs" onclick="showConfirmMsg('<?php echo URL_ADMIN; ?>', 'product', <?php echo $value['id'] ?>)">Excluir</a>
                                </td>

                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>  
    </div>
</div>    
<script src="<?php echo URL_ADMIN;?>public/js/JqueryMask/Masks/price_mask.js" type="text/javascript" charset="UTF-8"></script>