<form method="post"  enctype="multipart/form-data" action="<?php echo URL_ADMIN; ?>product/editSave/<?php echo $this->product['id']; ?>">
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12" >
                                <a><b>Editar Produto</b></a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" placeholder="Nome do Produto" class="form-control" name="name" value="<?php echo $this->product['name']; ?>" required/>
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Preço (R$)" class="form-control money" name="price" value="<?php echo $this->product['price']; ?>" required/>
                                </div>
                                <div class="form-group">
                                    <textarea type="text" style="resize:vertical" placeholder="Descrição" class="form-control" name="description" required><?php echo $this->product['description']; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Quantidade em estoque" class="form-control" name="quantity" value="<?php echo $this->product['quantity']; ?>"/>
                                </div>
                                <div class="form-group">
                                    <select name="prd_categories_id" placeholder="Categoria" class="form-control" required>
                                        <?php
                                        foreach ($this->prdCategoryList as $key => $value)
                                        {
                                            ?>
                                            <option 
                                                <?php echo ($this->product['prd_categories_id'] == $value['id']) ? 'selected' : ''?>
                                                value="<?php echo $value['id'];?>"><?php echo $value['name'];?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div> 
                                <?php if ($this->product['path1'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->product['path1']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem Principal" class="form-control" name="imagem1"></input>
                                </div>
                                <?php if ($this->product['path2'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->product['path2']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 2" class="form-control" name="imagem2" value="<?php echo $this->product['path2']; ?>"></input>
                                </div>
                                <?php if ($this->product['path3'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->product['path3']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 3" class="form-control" name="imagem3" value="<?php echo $this->product['path3']; ?>"></input>
                                </div>
                                <?php if ($this->product['path4'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->product['path4']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 4" class="form-control" name="imagem4" value="<?php echo $this->product['path4']; ?>"></input>
                                </div>
                                <?php if ($this->product['path5'] != '') {?>
                                <img style="max-width: 100px;" src="<?php echo URL_ADMIN . "../public/images/upload/" . $this->product['path5']; ?>"/>
                                <?php } ?>
                                <div class="form-group">
                                    <input type="file" placeholder="Imagem 5" class="form-control" name="imagem5" value="<?php echo $this->product['path5']; ?>"></input>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Gravar">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>
<script src="<?php echo URL_ADMIN;?>public/js/JqueryMask/Masks/price_mask.js" type="text/javascript" charset="UTF-8"></script>
