<?php

class AllOrders_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();

    }

    public function orderList()
    {
        return $this->db->selectAll('SELECT id, date, users_id, or_status_id FROM orders');
    }

}
