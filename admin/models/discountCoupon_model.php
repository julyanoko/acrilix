<?php

class DiscountCoupon_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function DiscountCouponList()
    {
        return $this->db->selectAll('SELECT id, code, percentage FROM discount_coupons');        
    }
    
    public function getDiscountCoupon($id)
    {
        return $this->db->select('SELECT id, code, percentage FROM discount_coupons where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('code' => $data['code'],
                        'percentage' => $data['percentage']);
        $this->db->insert('discount_coupons', $values);
    }
    
    public function editSave($data)
    {
        $values = array('code' => $data['code'],
                        'percentage' => $data['percentage']);
        $this->db->update('discount_coupons', $values, "`id` = {$data['id']}");
    }
    
    public function delete($id)
    {
        $this->db->delete('discount_coupons', "id = '$id'");
    }
}