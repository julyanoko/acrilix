<?php

class ReportSales_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function listReport()
    {
        $num_vendas = $this->db->select('SELECT count(*) as num_vendas FROM orders')['num_vendas'];
        $mais_vendido = $this->db->selectAll('SELECT products.name AS mais_vendido, '
                                            . 'count(orders_products.products_id) + orders_products.quantity AS soma '
                                            . 'FROM orders_products, products '
                                            . 'WHERE products.id = orders_products.products_id '
                                            . 'ORDER BY soma, mais_vendido');
        
        $mais_vendido = $this->db->selectAll('SELECT products.name AS mais_vendido, '
                                            . 'count(orders_products.products_id) + orders_products.quantity AS soma '
                                            . 'FROM orders_products, products '
                                            . 'WHERE products.id = orders_products.products_id '
                                            . 'ORDER BY soma, mais_vendido');
        
        $valor_arrecadado = $this->db->selectAll('SELECT sum(products.price * orders_products.quantity) as soma '
                                                . 'FROM orders_products, products '
                                                . 'WHERE orders_products.products_id = products.id');
        
        
        $mais_vendido_nm = "";
        $mais_vendido_qt = 0;
        $valor_arrecadado_qt = 0;
        if(isset($mais_vendido[0]['mais_vendido']))
        {
            $mais_vendido_nm = $mais_vendido[0]['mais_vendido'];
            $mais_vendido_qt = $mais_vendido[0]['soma'];
        }
        if (isset($mais_vendido[0]['soma'])) {
            $valor_arrecadado_qt = $valor_arrecadado[0]['soma'];
        }
        
        $data = array(
            'num_vendas' => 
                    array('desc' => 'Número total de pedidos',
                          'val' => $num_vendas)
                          ,
            'mais_vendido' => 
                    array('desc' => 'Produto mais vendido',
                          'val' => $mais_vendido_nm)
                          ,
            'mais_vendido_qt' => 
                    array('desc' => 'Quantidade vendida deste produto',
                          'val' => $mais_vendido_qt)
                          ,
            'valor_arrecadado' => 
                    array('desc' => 'Valor total arrecadado de todas as vendas',
                          'val' => $valor_arrecadado_qt)
                      );
        return $data;        
    }
}