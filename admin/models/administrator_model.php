<?php

class Administrator_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function administratorList()
    {
        return $this->db->selectAll('SELECT id, name, email FROM administrators ORDER BY id');        
    }
    
    public function getAdministrator($id)
    {
        return $this->db->select('SELECT id, name, email FROM administrators where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('email' => $data['email'],
                        'name' => $data['name'],
                        'password' => Hash::create($data['password']));
        if(!$this->db->insert('administrators', $values))
        {
            if ($this->db->getError() == '23000')
            {
                Msg::msgErro('Email já cadastrado!');
                header('location: '.URL_ADMIN.'administrator/add');
                return false;
            }
        }
        return true;
    }
    
    public function editSave($data)
    {
        $values = array('email' => $data['email'],
                        'name' => $data['name']);
        if (!$this->db->update('administrators', $values, "`id` = {$data['id']}"))
        {
            if ($this->db->getError() == '23000')
            {
                Msg::msgErro('Email já cadastrado!');
                header('location: '.URL_ADMIN.'administrator/edit/' . $data['id']);
                return false;
            }
        }
        return true;
    }
    
    public function delete($id)
    {
        $this->db->delete('administrators', "id = '$id'");
    }
}
