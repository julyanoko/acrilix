<?php

class User_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function userList()
    {
        return $this->db->selectAll('SELECT id, name, email FROM users ORDER BY id');        
    }
    
    public function getUser($id)
    {
        return $this->db->select('SELECT id, name, email FROM users where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('email' => $data['email'],
                        'name' => $data['name'],
                        'password' => Hash::create($data['password']));
        if(!$this->db->insert('users', $values))
        {
            if ($this->db->getError() == '23000')
            {
                Msg::msgErro('Email já cadastrado!');
                header('location: '.URL_ADMIN.'user/add');
                return false;
            }
        }
    }
    
    public function editSave($data)
    {
        $values = array('email' => $data['email'], 'name' => $data['name']);
        if (!$this->db->update('users', $values, "`id` = {$data['id']}"))
        {
            if ($this->db->getError() == '23000')
            {
                Msg::msgErro('Email já cadastrado!');
                header('location: '.URL_ADMIN.'user/edit/'.$data['id']);
                return false;
            }
        }
    }
    
    public function delete($id)
    {
        $this->db->delete('users', "id = '$id'");
    }
}