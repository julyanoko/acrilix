<?php

class Product_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function productList()
    {
        return $this->db->selectAll('SELECT products.id, products.name as name, products.quantity as quantity, '
                                    . 'products.description as description, products.price, '
                                  . 'products.prd_categories_id, prd_categories.name as prd_categories_name '
                                  . 'FROM products, prd_categories '
                                  . 'WHERE products.prd_categories_id = prd_categories.id');      
    }
    
    public function prdCategoryList()
    {
        return $this->db->selectAll('SELECT id, name FROM prd_categories');        
    }
    
    public function getProduct($id)
    {
        return $this->db->select('SELECT id, name, quantity, description, price, prd_categories_id, path1, path2, path3, path4, path5 FROM products where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('name' => $data['name'],
                        'prd_categories_id' => $data['prd_categories_id'],
                        'description' => $data['description'],
                        'quantity' => $data['quantity'],
                        'price' => $data['price'],
                        'path1' => $data['path1'],
                        'path2' => $data['path2'],
                        'path3' => $data['path3'],
                        'path4' => $data['path4'],
                        'path5' => $data['path5'],
                        'administrators_id' => Session::get('adm_logged')['id']);
        
        $this->db->insert('products', $values);
    }
    
    public function editSave($data)
    {
        $values = array('name' => $data['name'],
                        'prd_categories_id' => $data['prd_categories_id'],
                        'description' => $data['description'],
                        'quantity' => $data['quantity'],
                        'price' => $data['price'],
                        'path1' => $data['path1'],
                        'path2' => $data['path2'],
                        'path3' => $data['path3'],
                        'path4' => $data['path4'],
                        'path5' => $data['path5'],
                        'administrators_id' => Session::get('adm_logged')['id']);
        
        if ($values['path1'] == '')
        {
            unset($values['path1']);
        }
        if ($values['path2'] == '')
        {
            unset($values['path2']);
        }
        if ($values['path3'] == '')
        {
            unset($values['path3']);
        }
        if ($values['path4'] == '')
        {
            unset($values['path4']);
        }
        if ($values['path5'] == '')
        {
            unset($values['path5']);
        }
        
        $this->db->update('products', $values, "`id` = {$data['id']}");
    }
    
    public function delete($id)
    {
        $this->db->delete('products', "id = '$id'");
    }
}