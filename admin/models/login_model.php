<?php

class Login_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        if (!isset($_POST['email']))
        {
            Msg::msgErro('Administrador não cadastrado!');
            header('location: '.URL_ADMIN.'login');
            return;
        }

        $sth = $this->db->prepare("SELECT id, name, email, password FROM administrators WHERE email = :email AND password = :password");
        $sth->execute(array(
                            ':email' => $_POST['email'],
                            ':password' => Hash::create($_POST['password'])
        ));

        $data = $sth->fetch();

        $count = $sth->rowCount();
        if ($count > 0)
        {
            Msg::msgSuccess('Login efetuado com sucesso!');
            // Login
            Session::set('adm_logged', $data);
            header('location: ' . URL_ADMIN . 'index');
            return;
        }
        else
        {
            Msg::msgErro('Administrador não cadastrado!');
            header('location: ' . URL_ADMIN . 'login');
            return;
        }
    }
}
