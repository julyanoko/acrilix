<?php

class PrdCategory_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function prdCategoryList()
    {
        return $this->db->selectAll('SELECT id, name FROM prd_categories');        
    }
    
    public function getPrdCategory($id)
    {
        return $this->db->select('SELECT id, name FROM prd_categories where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('name' => $data['name']);
        $this->db->insert('prd_categories', $values);
    }
    
    public function editSave($data)
    {
        $values = array('name' => $data['name']);
        $this->db->update('prd_categories', $values, "`id` = {$data['id']}");
    }
    
    public function delete($id)
    {
        $this->db->delete('prd_categories', "id = '$id'");
    }
}