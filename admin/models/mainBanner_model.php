<?php

class MainBanner_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function mainBannerList()
    {
        return $this->db->selectAll('SELECT id, name, img_url, path '
                                  . 'FROM main_banners');      
    }
    
    public function getBanner($id)
    {
        return $this->db->select('SELECT id, name, img_url, path FROM main_banners where id = :id', array(':id' => $id));
    }
    
    public function create($data)
    {
        $values = array('name' => $data['name'],
                        'img_url' => $data['img_url'],
                        'path' => $data['path']);
        
        $this->db->insert('main_banners', $values);
    }
    
    public function editSave($data)
    {
        $values = array('name' => $data['name'],
                        'img_url' => $data['img_url'],
                        'path' => $data['path']);
        
        if ($values['path'] == '')
        {
            unset($values['path']);
        }
        
        $this->db->update('main_banners', $values, "`id` = {$data['id']}");
    }
    
    public function delete($id)
    {
        $this->db->delete('main_banners', "id = '$id'");
    }
}