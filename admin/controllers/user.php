<?php

class User extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function index()
    {
        $this->view->userList = $this->model->userList();
        $this->view->title = 'Usuários';
        $this->view->render('user/index');
    }

    public function create()
    {
        if (!isset($_POST['email']))
        {
            Msg::msgErro('Erro no cadastro do usuário!');
            header('location: '. URL_ADMIN .'user');
            return;
        }

        $confirm_password = $_POST['confirm_password'];
        $password = $_POST['password'];

        if ($password === $confirm_password)
        {
            $data = array('email' => $_POST['email'],
                          'name' => $_POST['name'],
                          'password' => $_POST['password']);

            if($this->model->create($data) !== false)
            {
                Msg::msgSuccess('Cadastro Realizado');
                header('location: '. URL_ADMIN .'user');
                return;
            }
        }
        else
        {
            Msg::msgErro('As senhas não conferem!');
            header('location: '. URL_ADMIN .'user/add');
            return;
        }
    }

    public function edit($id = 0)
    {
        if ($id == 0)
        {
            header('location: '. URL_ADMIN .'user');
            return;
        }
        else
        {
            $this->view->title = 'Editar';
            $this->view->user = $this->model->getUser($id);
            if ($this->view->user == null)
            {
                header('location: '. URL_ADMIN .'user');
                return;
            }
            else
            {
                $this->view->render('user/edit');
                return;
            }
        }
    }
    public function add()
    {
        $this->view->title = 'Adicionar';
        unset($this->view->user);
        $this->view->render('user/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar o usuário!');
            header('location: '. URL_ADMIN .'user');
            return false;
        }

        $data = array('id' => $id,
                      'email' => $_POST['email'],
                      'name' => $_POST['name']);

        if($this->model->editSave($data) !== false)
        {
            Msg::msgSuccess('Editado com sucesso!');
            header('location: '. URL_ADMIN .'user');
            return;
        }
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'user');
    }
}
