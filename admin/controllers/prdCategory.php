<?php

class PrdCategory extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function index()
    {
        $this->view->prdCategoryList = $this->model->prdCategoryList();
        $this->view->title = 'Categorias';
        $this->view->render('prdcategory/index');
    }

    public function create()
    {
        if (!isset($_POST['name']))
        {
            Msg::msgErro('Erro no cadastro da categoria!');
            header('location: '. URL_ADMIN .'prdcategory');
            return;
        }

        $data = array('name' => $_POST['name'],
                      'description' => $_POST['description']);

        $this->model->create($data);
        Msg::msgSuccess('Cadastro Realizado!');
        header('location: '. URL_ADMIN .'prdcategory');
    }

    public function edit($id = 0)
    {
        $this->view->title = 'Editar';
        $this->view->prdCategory = $this->model->getPrdCategory($id);
        if ($this->view->prdCategory == null)
        {
            header('location: '. URL_ADMIN .'prdcategory');
        }
        else
        {
            $this->view->render('prdcategory/edit');
        }
    }

    public function add()
    {
        $this->view->title = 'Adicionar';
        unset($this->view->prdCategory);
        $this->view->render('prdcategory/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar a categoria!');
            header('location: '. URL_ADMIN .'prdcategory');
            return false;
        }

        $data = array('id' => $id,
                      'name' => $_POST['name']);

        Msg::msgSuccess('Editado com sucesso!');
        $this->model->editSave($data);
        header('location: '. URL_ADMIN .'prdcategory');
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'prdcategory');
    }
}
