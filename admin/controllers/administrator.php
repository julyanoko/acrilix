<?php

class Administrator extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function index()
    {
        $this->view->administratorList = $this->model->administratorList();
        $this->view->title = 'Administradores';
        $this->view->render('administrator/index');
    }

    public function create()
    {
        if (!isset($_POST['email']))
        {
            Msg::msgErro('Erro no cadastro do Administrador!');
            header('location: '. URL_ADMIN .'administrator');
            return;
        }

        $confirm_password = $_POST['confirm_password'];
        $password = $_POST['password'];

        if ($password === $confirm_password)
        {
            $data = array('email' => $_POST['email'],
                          'name' => $_POST['name'],
                          'password' => $_POST['password']);

            if($this->model->create($data) !== false)
            {
                Msg::msgSuccess('Cadastro Realizado');
                header('location: '. URL_ADMIN .'administrator');
                return;
            }
        }
        else
        {
            Msg::msgErro('As senhas não conferem!');
            header('location: '. URL_ADMIN .'administrator/add');
            return;
        }
    }

    public function edit($id = 0)
    {
        if ($id == 0)
        {
            header('location: '. URL_ADMIN .'administrator');
            return;
        }
        else
        {
            $this->view->title = 'Editar';
            $this->view->administrator = $this->model->getAdministrator($id);
            if ($this->view->administrator == null)
            {
                header('location: '. URL_ADMIN .'administrator');
                return;
            }
            else
            {
                $this->view->render('administrator/edit');
                return;
            }
        }
    }

    public function add()
    {
        $this->view->title = 'Adicionar';
        unset($this->view->administrator);
        $this->view->render('administrator/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar o administrador!');
            header('location: '. URL_ADMIN .'administrator');
            return false;
        }

        $data = array('id' => $id,
                      'name' => $_POST['name'],
                      'email' => $_POST['email']);

        if($this->model->editSave($data) !== false)
        {
            Msg::msgSuccess('Editado com sucesso!');
            header('location: '. URL_ADMIN .'administrator');
            return;
        }
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'administrator');
    }
}
