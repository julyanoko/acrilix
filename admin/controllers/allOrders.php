<?php

class AllOrders extends Controller
{
    function __construct()
    {
        parent::__construct();
        Auth::checkLogged();

    }

    function index()
    {
        $this->view->orderList = $this->model->orderList();
        $this->view->title = 'Todos os Pedidos';
        $this->view->render('AllOrders/index');
    }
    function details($id = 0)
    {
        if (isset($id) && $id != 0)
        {

            $this->view->title = 'Detalhes do pedido';
            $this->view->render('AllOrders/details');
            return;
        }
        else
        {
            Msg::msgErro('Pedido não encontrado!');
            header('location: '. URL .'AllOrders');
            return;
        }
    }
}
