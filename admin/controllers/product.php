<?php

class Product extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function index()
    {
        $this->view->productList = $this->model->productList();
        $this->view->title = 'Produto';
        $this->view->render('product/index');
    }

    public function create()
    {
        if (!isset($_POST['name']))
        {
            Msg::msgErro('Erro ao cadastrar um novo produto!');
            header('location: '. URL_ADMIN .'product/add');
            return false;
        }
        
        $novo_nome1 = "";
        if (($_FILES['imagem1']) && (!empty($_FILES['imagem1']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem1']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome1 = md5(time() + uniqid() + "1") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem1']['tmp_name'], $diretorio . $novo_nome1))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $novo_nome2 = "";
        if (($_FILES['imagem2']) && (!empty($_FILES['imagem2']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem2']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome2 = md5(time() + uniqid() + "2") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem2']['tmp_name'], $diretorio . $novo_nome2))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $novo_nome3 = "";
        if (($_FILES['imagem3']) && (!empty($_FILES['imagem3']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem3']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome3 = md5(time() + uniqid() + "3") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem3']['tmp_name'], $diretorio . $novo_nome3))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $novo_nome4 = "";
        if (($_FILES['imagem4']) && (!empty($_FILES['imagem4']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem4']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome4 = md5(time() + uniqid() + "4") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem4']['tmp_name'], $diretorio . $novo_nome4))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $novo_nome5 = "";
        if (($_FILES['imagem5']) && (!empty($_FILES['imagem5']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem5']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome5 = md5(time() + uniqid() + "5") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem5']['tmp_name'], $diretorio . $novo_nome5))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $data = array('name' => $_POST['name'],
                      'price' => $_POST['price'],
                      'quantity' => $_POST['quantity'],
                      'path1' => $novo_nome1,
                      'path2' => $novo_nome2,
                      'path3' => $novo_nome3,
                      'path4' => $novo_nome4,
                      'path5' => $novo_nome5,
                      'description' => $_POST['description'],
                      'prd_categories_id' => $_POST['prd_categories_id']);

        $this->model->create($data);
        Msg::msgSuccess('Cadastro Realizado');
        header('location: '. URL_ADMIN .'product');
    }

    public function edit($id = 0)
    {
        $this->view->title = 'Editar';
        $this->view->product = $this->model->getProduct($id);
        if ($this->view->product == null)
        {
            header('location: '. URL_ADMIN .'product');
        }
        else
        {
            $this->view->prdCategoryList = $this->model->prdCategoryList();
            $this->view->render('product/edit');
        }
    }
    public function add()
    {
        $this->view->title = 'Adicionar';
        $this->view->prdCategoryList = $this->model->prdCategoryList();
        $this->view->render('product/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar o produto!');
            header('location: '. URL_ADMIN .'product');
            return false;
        }

        $novo_nome1 = "";
        if (($_FILES['imagem1']) && (!empty($_FILES['imagem1']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem1']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome1 = md5(time() + uniqid() + "1") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem1']['tmp_name'], $diretorio . $novo_nome1))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $novo_nome2 = "";
        if (($_FILES['imagem2']) && (!empty($_FILES['imagem2']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem2']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome2 = md5(time() + uniqid() + "2") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem2']['tmp_name'], $diretorio . $novo_nome2))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $novo_nome3 = "";
        if (($_FILES['imagem3']) && (!empty($_FILES['imagem3']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem3']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome3 = md5(time() + uniqid() + "3") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem3']['tmp_name'], $diretorio . $novo_nome3))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $novo_nome4 = "";
        if (($_FILES['imagem4']) && (!empty($_FILES['imagem4']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem4']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome4 = md5(time() + uniqid() + "4") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem4']['tmp_name'], $diretorio . $novo_nome4))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $novo_nome5 = "";
        if (($_FILES['imagem5']) && (!empty($_FILES['imagem5']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem5']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome5 = md5(time() + uniqid() + "5") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem5']['tmp_name'], $diretorio . $novo_nome5))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $data = array('id' => $id,
                      'name' => $_POST['name'],
                      'price' => $_POST['price'],
                      'quantity' => $_POST['quantity'],
                      'path1' => $novo_nome1,
                      'path2' => $novo_nome2,
                      'path3' => $novo_nome3,
                      'path4' => $novo_nome4,
                      'path5' => $novo_nome5,
                      'description' => $_POST['description'],
                      'prd_categories_id' => $_POST['prd_categories_id']);

        Msg::msgSuccess('Editado com sucesso!');
        $this->model->editSave($data);
        header('location: '. URL_ADMIN .'product');
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'product');
    }
}
