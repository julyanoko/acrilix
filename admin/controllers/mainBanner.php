<?php

class MainBanner extends Controller
{
    function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    function index()
    {
        $this->view->mainBannerList = $this->model->mainBannerList();
        $this->view->title = 'Todos os Banners';
        $this->view->render('mainBanner/index');
    }
    
    public function create()
    {
        if (!isset($_POST['name']))
        {
            Msg::msgErro('Erro ao cadastrar um novo banner!');
            header('location: '. URL_ADMIN .'mainBanner/add');
            return false;
        }
        
        $novo_nome = "";
        if (($_FILES['imagem']) && (!empty($_FILES['imagem']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome = md5(time() + uniqid() + "1") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem']['tmp_name'], $diretorio . $novo_nome))
            {
                Msg::msgErro("Erro ao enviar arquivo");
                return false;
            }
        }

        $data = array('name' => $_POST['name'],
                      'img_url' => $_POST['img_url'],
                      'path' => $novo_nome);

        $this->model->create($data);
        Msg::msgSuccess('Cadastro Realizado');
        header('location: '. URL_ADMIN .'mainBanner');
    }

    public function edit($id = 0)
    {
        $this->view->title = 'Editar';
        $this->view->banner = $this->model->getBanner($id);
        if ($this->view->banner == null)
        {
            header('location: '. URL_ADMIN .'mainBanner');
        }
        else
        {
            $this->view->render('mainBanner/edit');
        }
    }
    public function add()
    {
        $this->view->title = 'Adicionar';
        $this->view->render('mainBanner/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar o banner!');
            header('location: '. URL_ADMIN .'mainBanner');
            return false;
        }

        $novo_nome = "";
        if (($_FILES['imagem']) && (!empty($_FILES['imagem']['name'])))
        {
            // pega a extensão do arquivo
            $extensao = explode('.', $_FILES['imagem']['name']);
            $extensao = '.' . strtolower(end($extensao));
            
            $novo_nome = md5(time() + uniqid() + "1") . $extensao;
            $diretorio = $_SERVER['DOCUMENT_ROOT'] ."/Acrilix/public/images/upload/";
            if (!move_uploaded_file($_FILES['imagem']['tmp_name'], $diretorio . $novo_nome))
            {
                Msg::msgErro("Erro ao enviar arquivo");
            }
        }

        $data = array('id' => $id,
                      'name' => $_POST['name'],
                      'img_url' => $_POST['img_url'],
                      'path' => $novo_nome);

        Msg::msgSuccess('Editado com sucesso!');
        $this->model->editSave($data);
        header('location: '. URL_ADMIN .'mainBanner');
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'mainBanner');
    }
}
