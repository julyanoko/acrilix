<?php

class Index extends Controller
{
    function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    function index()
    {
        $this->view->title = 'Administração';
        $this->view->render('index/index');
    }
}
