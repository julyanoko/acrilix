<?php

class DiscountCoupon extends Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    public function index()
    {
        $this->view->discountCouponList = $this->model->DiscountCouponList();
        $this->view->title = 'Cupons de Desconto';
        $this->view->render('discountCoupon/index');
    }

    public function create()
    {
        if (!isset($_POST['code']))
        {
            Msg::msgErro('Erro no cadastro do cupom de desconto!');
            header('location: '. URL_ADMIN .'discountCoupon');
            return;
        }

        $data = array('code' => $_POST['code'],
                      'percentage' => $_POST['percentage']);

        $this->model->create($data);
        Msg::msgSuccess('Cadastro Realizado!');
        header('location: '. URL_ADMIN .'discountCoupon');
    }

    public function edit($id = 0)
    {
        $this->view->title = 'Editar';
        $this->view->discountCoupon = $this->model->getDiscountCoupon($id);
        if ($this->view->discountCoupon == null)
        {
            header('location: '. URL_ADMIN .'discountCoupon');
        }
        else
        {
            $this->view->render('discountCoupon/edit');
        }
    }

    public function add()
    {
        $this->view->title = 'Adicionar';
        unset($this->view->discountCoupon);
        $this->view->render('discountCoupon/add');
    }

    public function editSave($id)
    {
        if (!isset($id))
        {
            Msg::msgErro('Erro ao editar o cupom de desconto!');
            header('location: '. URL_ADMIN .'discountCoupon');
            return false;
        }

        $data = array('id' => $id,
                      'code' => $_POST['code'],
                      'percentage' => $_POST['percentage']);

        Msg::msgSuccess('Editado com sucesso!');
        $this->model->editSave($data);
        header('location: '. URL_ADMIN .'discountCoupon');
    }

    public function delete($id)
    {
        $this->model->delete((int)$id);
        Msg::msgSuccess('Excluido com sucesso!');
        header('location: '. URL_ADMIN .'discountCoupon');
    }
}
