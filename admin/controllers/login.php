<?php

class Login extends Controller
{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        Auth::checkAlreadyLogged();
        $this->view->title = 'Login Administrador';
        $this->view->renderWithoutHeader('login/index');
    }

    function run()
    {
        Auth::checkAlreadyLogged();
        $this->model->run();
    }

    function logout()
    {
        Session::destroy('adm_logged');
        header('location: '.URL_ADMIN.'login');
        exit;
    }
}
