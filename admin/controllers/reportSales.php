<?php

class ReportSales extends Controller
{
    function __construct()
    {
        parent::__construct();
        Auth::checkLogged();
    }

    function index()
    {
        $this->view->listReport = $this->model->listReport();
        $this->view->title = 'Relatório de Vendas';
        $this->view->render('reportSales/index');
    }
}
