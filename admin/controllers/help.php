<?php

class Help extends Controller
{
    function __construct()
    {
        Auth::checkLogged();
        parent::__construct();
    }

    function index()
    {
        $this->view->title = 'Ajuda';
        $this->view->render('help/index');
    }
}
