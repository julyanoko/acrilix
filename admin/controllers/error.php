<?php
/*
 * Classe responsável por exibir todos os erros que ocorrerem no programa
 */
class Error extends Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    function index()
    {
        $this->view->title = 'Erro - 404';
        $this->view->msg = '#404 - Essa página não existe';
        $this->view->renderWithoutHeader('error/index');
    }    
    
    function error404()
    {
        $this->view->title = 'Erro - 404';
        $this->view->msg = '#404 - Essa página não existe';
        $this->view->renderWithoutHeader('error/index');
    }    
    
    function error403()
    {
        $this->view->title = 'Erro - 403';
        $this->view->msg = '#403 - Acesso negado';
        $this->view->renderWithoutHeader('error/index');
    }    
}
