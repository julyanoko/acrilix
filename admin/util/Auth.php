<?php

class Auth
{
    /*
     * Verifica se o cliente está logado
     * Se não retorna a tela de login de clientes
     */
     public static function checkLogged()
     {
         // Se não estiver logado retorna false
         if (!isset($_SESSION['adm_logged']))
         {
             Msg::msgErro('Você deve estar logado para acessar esta área!');
             // NÃO MUDE MAZUR PFVR
             unset($_SESSION['adm_logged']);
             header('location: '.URL_ADMIN.'login');
             exit;
         }
     }

     /*
      * Verifica se o administrador já está logado
      * Se já estiver retorna para a sua tela principal
      */
     public static function checkAlreadyLogged()
     {
         // Se não estiver logado retorna false
         if (isset($_SESSION['adm_logged']))
         {
             header('location: '.URL_ADMIN.'index');
             exit;
         }
     }




}
