<?php
/*
 * Classe responsável por carregar tudo que precisa ser utilizado por todo o site
*/

// Habilita a proteção contra XSS, se detectar que está sendo atacado o site não será renderizado
// Porque o modo foi definido como 'block'
header("X-XSS-Protection: 1; mode=block");
// Proteção contra 'Clickjacking'
header("X-Frame-Options: deny");
// Proteção contra 'downgrade attacks' e 'cookie hijacking' colocando um tempo máximo (em segundos)
// Faz com que o usuário não consiga snipar uma conexão entre um servidor e usuário atráves da sessão e também
// Protege contra downgrades de segurança (usar http quando se está habilitado o https, assim por diante)
header("Strict-Transport-Security: max-age=31536000; includeSubDomains");
// Muda o PIN das chaves públicas
header('Public-Key-Pins: pin-sha256="720d821dcc4c1da51c0fd7cc33fed163596583f899b6a2b06dfde2cfbba58684=";pin-sha256="966114ab6759cede0bc25fd5b1cf954decc686173d239e9a93cba9086082bc90=";includeSubDomains');
// Protege o site contra vunerabilidades no chrome e IE que permitem mudar declarações de segurança
header('X-Content-Type-Options: nosniff');

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);

ini_set('display_errors',0);
ini_set('display_startup_erros',0);
error_reporting(0);

//Carrega as configurações necessárias para o funcionamento do sistema
require 'config.php';


// Carrega as funções para verificar se está logado ou não
require 'util/Auth.php';

// Carrega as funções para gerenciar mensagens para o usuário
require 'util/Msg.php';

/*
 * Faz todos os requires da pasta libs apenas quando for usado
*/
function __autoload($class)
{
    require "libs/" . $class . ".php";
}

// Coloca o fuso horário como o de são paulo
date_default_timezone_set('America/Sao_Paulo');

Session::init();


$route = new Routes();
$route->init();
