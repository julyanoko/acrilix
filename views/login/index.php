      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <p><h2>Já possui conta? Entre</h2></p>
            <form id="login-form" action="<?php echo URL;?>login/run" method="post"
            role="form" style="display: block;">
              <div class="form-group">
                <div class="input-group">
   <div class="input-group-addon">
	<span class="glyphicon glyphicon-envelope"></span>
   </div>
                <input type="email" class="form-control" name="email" id="inputEmail"
                placeholder="Email" required="">
              </div>
            </div>
              <div class="form-group">
                <div class="input-group">
                <div class="input-group-addon">
                     <span class="glyphicon glyphicon-lock"></span>
                </div>
                <input type="password" class="form-control" autocomplete="off" name="password"
                id="inputPassword" placeholder="Senha" required="">
              </div></div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-sm-offset-3">
                    <input type="submit" name="login-submit" id="login-submit" tabindex="4"
                    class="form-control btn btn-login" value="ENTRAR">
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modalSenha" tabindex="5" class="forgot-password">Esqueci Minha Senha</a>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <p><h2>Não possui conta? Registre-se</h2></p>
            <form id="register-form" action="<?php echo URL; ?>login/create"
            method="post" role="form" style="display: block;">
              <div class="form-group">
                <input type="email" class="form-control" name="email" id="inputEmail"
                placeholder="Email" required="" tabindex="1">
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="name" id="inputName" placeholder="Nome"
                required="" tabindex="1">
              </div>
              <div class="form-group">
                <input type="password" class="form-control" autocomplete="off" tabindex="2"
                name="password" id="inputPassword" placeholder="Senha" required="">
              </div>
              <div class="form-group">
                <input type="password" name="confirm_password" autocomplete="off" id="confirm_password"
                tabindex="3" class="form-control" placeholder="Confirme a Senha" required="">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-6 col-sm-offset-3">
                    <input type="submit" name="register-submit" id="register-submit" tabindex="4"
                    class="form-control btn btn-register" value="Registrar">
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal fade" id="modalSenha" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Esqueci minha senha</h4>
              </div>
              <div class="modal-body">
                <p><input type="email" class="form-control" name="email" id="emailSenha"
                placeholder="E-mail" required="true"></p>
              </div>
              <div class="modal-footer">
                <input type="submit" name="register-submit" id="senha-submit"
                class="form-control btn btn-register" value="Enviar">
              </div>
            </div>
          </div>
        </div>
<script type='text/javascript' src="<?php echo URL; ?>public/js/esqueci_senha.js" charset="UTF-8"></script>
