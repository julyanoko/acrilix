<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang='pt-br'>
    <head>
        <!-- META TAG'S -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="description" content="Acrilix - Peças em acrilico." />
        <meta name="keywords" content="Acrilix - Peças em acrilico."/>
        <meta name="robots" content="index, follow">
        <meta http-equiv="content-language" content="pt-br"/>

        <!-- Title (padrão: Acrilix) -->
        <title><?php echo isset($this->title) ? $this->title : 'Acrilix'; ?></title>
        <!-- Icon da barra de endereços / abas -->
        <link rel="shortcut icon" href="<?php echo URL; ?>public/images/favicon.ico" type="image/x-icon" />
        <link href="<?php echo URL; ?>public/css/w3.css" rel="stylesheet" type="text/css"/>
        <!-- jquery -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/jquery.js" charset="UTF-8"></script>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/bootstrap.css" charset="UTF-8">
        <link rel="stylesheet" href="<?php echo URL; ?>public/fonts/glyphicons-halflings-regular.svg" charset="UTF-8">
        <script type='text/javascript' src="<?php echo URL; ?>public/js/bootstrap.js" charset="UTF-8"></script>
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/ie10-viewport-bug-workaround.css" charset="UTF-8">
        <!-- Sweet Alert-->
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/SweetAlert/sweetalert2.css" charset="UTF-8">
        <script type='text/javascript' src="<?php echo URL; ?>public/js/SweetAlert/sweetalert2.min.js" charset="UTF-8"></script>
        <!-- Jquery Mask -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/JqueryMask/jquery.mask.min.js" charset="UTF-8"></script>
        <!-- javascript comuns em todas as telas -->
        <script type='text/javascript' src="<?php echo URL; ?>public/js/msg.js" charset="UTF-8"></script>
        <script type='text/javascript' src="<?php echo URL; ?>public/js/ie-emulation-modes-warning.js" charset="UTF-8"></script>
        <!-- css comuns em todas as telas -->
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/style.css" charset="UTF-8">

        <script src="<?php echo URL; ?>public/js/Slider/jssor.slider-21.1.6.min.js" type="text/javascript"></script>
        <script src="<?php echo URL; ?>public/js/Analytics/analyticstracking.js" type="text/javascript"></script>
        <?php
        $user_logged = SESSION::get('user_logged');
        $username = $user_logged['name'];
        $qtdCarrinho = 0;
        foreach($_SESSION['carrinho'] as $i){
          $qtdCarrinho += $i;
        }
        ?>
        <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo URL; ?>index" title="Acrilix">
                    <img style="max-width:100px; margin-top: -7px;" src="<?php echo URL; ?>public/images/logo.png"></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                    <li><a href="<?php echo URL; ?>listProduct">Todos os Produtos</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php if (Session::get('user_logged') !== NULL) : ?>
                        <li><a href="<?php echo URL; ?>cart"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span class="badge badge-notify" id="qtdCarrinho"><?= $qtdCarrinho ?></span></a></li>
                        <li><a href="<?php echo URL; ?>index"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Olá <?php echo $username ?>!</a></li>
                        <li><a href="<?php echo URL; ?>myOrders"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Meus Pedidos</a></li>
                        <li><a href="<?php echo URL; ?>login/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Sair</a></li>
                    <?php else: ?>
                        <li><a href="<?php echo URL; ?>cart"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span><span class="badge badge-notify" id="qtdCarrinho"><?= $qtdCarrinho ?></span></a></li>
                        <li><a href="<?php echo URL; ?>login"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Entrar/Registrar</a></li>
                        <?php endif; ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
