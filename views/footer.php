<style>
.transparencia {
     filter:alpha(opacity=50);
     opacity: 0.5;
     -moz-opacity:0.5;
     -webkit-opacity:0.5;
}
.rodape{
    border-color: #000000;
    border-top: 1px solid;
     filter:alpha(opacity=50);
     opacity: 0.5;
     -moz-opacity:0.5;
     -webkit-opacity:0.5;
    
}

</style>
<!-- Footer -->
<footer class="cor rodape" style="margin-top: 50px; border-color: #000000; " >
    <div class="col-md-12 transparencia" style="height: 100px; filter: alpha(opacity=100); position: relative"   >
       <div class="col-lg-12" align="center" > 
           <div class="col-lg-3">  
           <h5>QUEM SOMOS</h5>
           </div>
            <div class="col-lg-3">  
           <h5>COMO COMPRAR</h5>
           </div>
            <div class="col-lg-3">  
           <h5>FALE CONOSCO</h5>
           </div>
            <div class="col-lg-3">  
           <h5>ENTREGA E PRAZO</h5>
           </div>
           <div class="col-lg-12" align="center">            
            <h4 >Copyright &copy; Acrilix 2016 | Desenvolvido por <a href="http://mbcsistemas.com.br/">MBCSistemas</a></h4>
          </div>
       </div>    
        
    </div>
</footer>

</html>

<!-- Trecho de código responsável por mandar mensagens para a tela do usuário -->

<?php
if (isset($_SESSION['erro']))
{
?>
    <script type='text/javascript' charset="UTF-8" >showErrorMsg(<?php echo json_encode($_SESSION['erro']);?>);</script>
<?php
    Session::destroy('erro');
}
if (isset($_SESSION['success']))
{
?>
    <script type='text/javascript' charset="UTF-8" >showSuccessMsg(<?php echo json_encode($_SESSION['success']);?>);</script>
<?php
    Session::destroy('success');
}
