<div class="container">
    <div class="row" style="margin-top: 20px; ">
        <div class="col-xs-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-xs-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Carrinho de Compras</h5>
                            </div>
                            <div class="col-xs-6">
                                <a href="<?php echo URL . 'listproduct' ?>" type="button" class="btn btn-primary btn-sm btn-block">
                                    <span class="glyphicon glyphicon-share-alt"></span> Continuar comprando
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <?php
                    $sum_column = 0;
                    $sum_total = 0;
                    $find = false;
                    foreach ($_SESSION['carrinho'] as $idx_carrinho => $carrinho)
                    {
                        foreach ($this->productList as $idx_produto => $produto)
                        {
                            // Se o produto que está na lista não for o mesmo que o produto ele continua o for
                            if ($produto['id'] != $idx_carrinho)
                            {
                                continue;
                            }
                            $find = true;

                            
                            $preco = $produto['price'];
                            $preco = str_replace(",",".",$preco);
                            $preco = number_format((float)$preco, 2);
                            
                            $car = str_replace(",", ".", $carrinho);

                            
                            $sum_column = $preco * $car;
                            
                            $s_tot = str_replace(",", ".", $sum_total);
                            
                            $sum_total = $sum_column + $s_tot;
                            $sum_total = number_format($sum_total,2);
                            $sum_column = number_format($sum_column,2);
                            ?>
                            <div class="row">                                
                                <div class="col-xs-2"><img class="img-responsive" src="<?php echo URL . "public/images/upload/" . $produto['path1']; ?>">

                                </div>
                                <div class="col-xs-3">
                                        <h4 class="product-name">
                                    <a href="<?php echo URL;?>productDesc/Index/<?php echo $produto['id'] ?>"
                                            <strong>
                                                <?php echo $produto['name'] ?>
                                            </strong>
                                    </a>
                                        </h4>
                                       <h4>
                                            <small>
                                                <?php echo $produto['description'] ?>
                                            </small>
                                        </h4>
                                </div>
                                <div class="col-xs-7">
                                    <div class="col-xs-3 text-right">
                                        <h6><strong>R$<label class="money"><?php echo $preco ?></label> <span class="text-muted">x</span></strong></h6>
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" disabled="" class="form-control input-sm" value="<?php echo $carrinho ?>">
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="<?php echo URL . 'order/edit/1/' . $produto['id'] ?>" type="button" class="btn btn-link btn-xs">
                                            <span class="glyphicon glyphicon-plus"> </span>
                                        </a>
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="<?php echo URL . 'order/edit/2/' . $produto['id'] ?>" type="button" class="btn btn-link btn-xs">
                                            <span class="glyphicon glyphicon-minus"> </span>
                                        </a>
                                    </div>
                                    <div class="col-xs-4 text-left">
                                        <h6><strong><span class="text-muted">= </span>
                                                R$<label class="money" id="item_result" name="item_result"><?php echo $sum_column?></label>
                                            </strong>
                                        </h6>
                                    </div>
                                    <div class="col-xs-1">
                                        <a href="<?php echo URL . 'order/delete/' . $produto['id'] ?>" type="button" class="btn btn-link btn-xs">
                                            <span class="glyphicon glyphicon-trash"> </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <?php
                            // Não precisa continuar o for de produtos se ele já foi encontrado
                            break;
                        }
                    }
                    if (!$find)
                    {
                        ?>
                        <div class = "row">
                            <div class = "text-center">
                                <div class = "col-xs-12">
                                    <h6 class = "text-center">Não existem produtos cadastrados!</h6>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="panel-footer">
                    <div class="row text-center">
                        <div class="col-xs-9">
                            <h4 class="text-right">Total <strong>R$<label class="money" id="resultado" name="resultado"><?php echo $sum_total?></label> </strong></h4>
                        </div>
                        <div class="col-xs-3">
                            <a href="order/buy" type="button" class="btn btn-success btn-block">
                                Finalizar Compra
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo URL; ?>public/js/JqueryMask/Masks/price_mask.js" type="text/javascript" charset="UTF-8"></script>