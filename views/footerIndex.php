     <footer class="footer">
        <div class="footer-stuff">
            Copyright &copy; <?= (date('Y') == 2016) ? date('Y') : ("2016" . " - " . date('Y')) ?>. Acrilix. Todos os Direitos Reservados | Desenvolvido por <a href="#">Mbc Sistemas</a>
        </div>
     </footer>
</html>

<!-- Trecho de código responsável por mandar mensagens para a tela do usuário -->

<?php
if (isset($_SESSION['erro']))
{
?>
    <script type='text/javascript' charset="UTF-8" >showErrorMsg(<?php echo json_encode($_SESSION['erro']);?>);</script>
<?php
    Session::destroy('erro');
}
if (isset($_SESSION['success']))
{
?>
    <script type='text/javascript' charset="UTF-8" >showSuccessMsg(<?php echo json_encode($_SESSION['success']);?>);</script>
<?php
    Session::destroy('success');
}
