<div class="container">
    <div class="col-md-2">
      <div class="row">
        <div class="panel panel-primary">

    <div class="panel-heading">CATEGORIAS</div>
            <ul class="list-group">
            <?php
            foreach ($this->prdCategoryList as $key => $value) {
                ?>
                <li class="list-group-item"><a href="<?php echo URL; ?>listProduct/filter/<?php echo $value['id'] ?>"><?php echo $value['name'] ?></a></li>
                <?php
            }
            ?>
          </ul>
          </div>
        </div>
      </div>
    <div class="col-md-10">
        <div class="row text-center">

            <?php
            foreach ($this->productList as $key => $value) {
                ?>
                <div class="col-md-4 col-sm-6 hero-feature">
                    <div class="thumbnail">
                        <img src="<?php echo URL . "/public/images/upload/" . $value['path1']; ?>" />
                        <div class="caption" >
                            <h3><a href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>"> <?php echo $value['name'] ?></a></h3>
                            <p><h2>R$<label class="money"><?php echo $value['price'] ?></label></h2>
                            </p>
                            <p>
                                <?php
                                if ($value['quantity'] > 0)
                                {
                                    ?>
                                        <input type="button" value="COMPRAR D" id="botaoComprar" class="btn btn-primary" onclick="adicionarAoCarrinho(<?= $value['id'] ?>)" />
                                        <a href="<?php echo URL; ?>cart/add/<?php echo $value['id'] ?>" class="btn btn-primary">COMPRAR</a>
                                    <?php
                               }
                               else {
                                   ?>
                                        <a class="btn btn-primary disabled" href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>">INDISPONÍVEL</a>
                                   <?php
                               }
                               ?>

                                <a href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>" class="btn btn-default">DETALHES</a>
                            </p>
                        </div>
                    </div>
                </div>
    <?php
}
?>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-body">
          <div class="swal2-content" style="display: block;">Adicionado ao carrinho</div>
        </div>
        <br />
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <br /><br />
      </div>
    </div>
  </div>


      </div>
      </div>
    </div>
    <div align="center">
        <!--<h1 >Todos produtos</h1>-->


    <script src="<?php echo URL; ?>public/js/JqueryMask/Masks/price_mask.js" type="text/javascript" charset="UTF-8"></script>
</div>

<script>
  function adicionarAoCarrinho(idProduto){
    var params = {
      produto : idProduto
    }
    $.post( "cart/addDinamico", params, function(){}, 'json')
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#qtdCarrinho').html(data['qtdCarrinho']);
    }, "json");
  }

</script>
