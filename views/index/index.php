<div class="container">
  <div class="row">


    <?php
    if (count($this->mainBannerList) > 0) {
        ?>


        <!-- #region Jssor Slider Begin -->
        <!-- Generator: Jssor Slider Maker -->
        <!-- Source: http://www.jssor.com/jefersonsccp/full-width-slider.slider -->
        <!-- This code works without jquery library. -->
        <script type="text/javascript">
            jssor_1_slider_init = function () {

                var jssor_1_options = {
                    $AutoPlay: true,
                    $SlideDuration: 800,
                    $SlideEasing: $Jease$.$OutQuint,
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$
                    },
                    $BulletNavigatorOptions: {
                        $Class: $JssorBulletNavigator$
                    }
                };

                var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

                /*responsive code begin*/
                /*you can remove responsive code if you don't want the slider scales while window resizing*/
                function ScaleSlider() {
                    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                    if (refSize) {
                        refSize = Math.min(refSize, 1920);
                        jssor_1_slider.$ScaleWidth(refSize);
                    } else {
                        window.setTimeout(ScaleSlider, 30);
                    }
                }
                ScaleSlider();
                $Jssor$.$AddEvent(window, "load", ScaleSlider);
                $Jssor$.$AddEvent(window, "resize", ScaleSlider);
                $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
                /*responsive code end*/
            };
        </script>
        <style>
            /* jssor slider bullet navigator skin 05 css */
            /*
            .jssorb05 div           (normal)
            .jssorb05 div:hover     (normal mouseover)
            .jssorb05 .av           (active)
            .jssorb05 .av:hover     (active mouseover)
            .jssorb05 .dn           (mousedown)
            */
            .jssorb05 {
                position: absolute;
            }
            .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                position: absolute;
                /* size of bullet elment */
                width: 16px;
                height: 16px;
                background: url('/theme/img/b05.png') no-repeat;
                overflow: hidden;
                cursor: pointer;
            }
            .jssorb05 div { background-position: -7px -7px; }
            .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
            .jssorb05 .av { background-position: -67px -7px; }
            .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

            /* jssor slider arrow navigator skin 22 css */
            /*
            .jssora22l                  (normal)
            .jssora22r                  (normal)
            .jssora22l:hover            (normal mouseover)
            .jssora22r:hover            (normal mouseover)
            .jssora22l.jssora22ldn      (mousedown)
            .jssora22r.jssora22rdn      (mousedown)
            .jssora22l.jssora22lds      (disabled)
            .jssora22r.jssora22rds      (disabled)
            */
            .jssora22l, .jssora22r {
                display: block;
                position: absolute;
                /* size of arrow element */
                width: 40px;
                height: 58px;
                cursor: pointer;
                background: url('http://www.jssor.com/theme/img/a22.png') center center no-repeat;
                overflow: hidden;
                z-index: 91;
            }
            .jssora22l { background-position: -10px -31px; }
            .jssora22r { background-position: -70px -31px; }
            .jssora22l:hover { background-position: -130px -31px; }
            .jssora22r:hover { background-position: -190px -31px; }
            .jssora22l.jssora22ldn { background-position: -250px -31px; }
            .jssora22r.jssora22rdn { background-position: -310px -31px; }
            .jssora22l.jssora22lds { background-position: -10px -31px; opacity: .3; pointer-events: none; }
            .jssora22r.jssora22rds { background-position: -70px -31px; opacity: .3; pointer-events: none; }
        </style>
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('/theme/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">



                <?php
                foreach ($this->mainBannerList as $key => $value) {
                    ?>
                    <div data-p="225.00">
                        <a href="<?php echo $value['img_url']; ?>">
                            <img data-u="image" src="<?php echo URL . "public/images/upload/" . $value['path']; ?>" />

                            <!-- <div style="position: absolute; font-size: 30px; color: #ffffff; line-height: 38px;">
                              <?php echo $value['name']; ?>
                            </div> -->
                        </a>
                    </div>
                <?php
                }
                ?>




            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <span data-u="arrowleft" class="jssora22l" style="top:0px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
            <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
        </div>
        <script type="text/javascript">jssor_1_slider_init();</script>
        <!-- #endregion Jssor Slider End -->



    <?php
}
?>










    <!-- Jumbotron Header
    <header class="jumbotron hero-spacer">
        <h1>Seja Bem Vindo!</h1>
        <p>Estamos a disposição para oferecer seu kit de banheiro, fique a vontade faça seu pedido :D</p>
    </header>
    <hr>
    -->
    <!-- Title -->
        <div class="col-lg-12">
            <h3>PRODUTOS EM DESTAQUE</h3>
        </div>
    </div>
    <!-- /.row -->

    <!-- Page Features -->
    <div class="row text-center">
<?php
foreach ($this->productList as $key => $value) {
    for ($i = 1; $i <= 5; $i++) {
        if (isset($value['path' . $i])) {
            ?>
                    <div class="col-md-3 col-sm-6 hero-feature">
                        <div class="thumbnail">
                            <img src="<?php echo URL . "public/images/upload/" . $value['path' . $i]; ?>" />
                            <div class="caption" >
                                <h3><a href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>"> <?php echo $value['name'] ?></a></h3>
                                <p><h2>R$<label class="money"><?php echo $value['price'] ?></label></h2>
                                </p>
                                <p>
            <?php
            if ($value['quantity'] > 0) {
                ?>
                                        <input type="button" value="COMPRAR D" id="botaoComprar" class="btn btn-primary" onclick="adicionarAoCarrinho(<?= $value['id'] ?>)" />
                                        <a href="<?php echo URL; ?>cart/add/<?php echo $value['id'] ?>" class="btn btn-primary">COMPRAR</a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class="btn btn-primary disabled" href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>">INDISPONÍVEL</a>
                                        <?php
                                    }
                                    ?>

                                    <a href="<?php echo URL; ?>productDesc/Index/<?php echo $value['id'] ?>" class="btn btn-default">DETALHES</a>
                                </p>
                            </div>
                        </div>
                    </div>
                                    <?php
                                }
                            }
                            ?>
<?php } ?>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
              <div class="swal2-content" style="display: block;">Adicionado ao carrinho</div>
            </div>
            <br />
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            <br /><br />
          </div>
        </div>
      </div>

    </div>
</div>

<!-- /.row -->

<script>
  function adicionarAoCarrinho(idProduto){
    var params = {
      produto : idProduto
    }
    $.post( "cart/addDinamico", params, function(){}, 'json')
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#qtdCarrinho').html(data['qtdCarrinho']);
    }, "json");
  }

</script>
