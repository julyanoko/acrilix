<div align="center">
    <h1>Meus Pedidos</h1>
    <div>
        <div class="container" style="padding-top:30px">
            <div  class="table-responsive" >
                <table id="tb_pais" class="table table-striped table-bordered table-hover" role="grid" cellspacing="0" width="100%">

                    <thead class="thead-inverse">
                        <tr>
                            <th id="id" class="col-md-3">Código</th>
                            <th id="date" class="col-md-4">Data</th>
                            <th id="or_status_id" class="col-md-5">Status</th>
                            <!--<th id="details" class="col-md-2">Outros</th>-->
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th id="id" class="col-md-3">Código</th>
                            <th id="date" class="col-md-4">Data</th>
                            <th id="or_status_id" class="col-md-5">Status</th>
                            <!--<th id="details" class="col-md-2">Outros</th>-->
                        </tr>
                    </tfoot>   
                    <?php
                    $logged_data = Session::get('user_logged');
                    foreach ($this->orderList as $key => $value)
                    {
                        if ($value['users_id'] != Session::get('user_logged')['id'])
                        {
                            continue;
                        }
                        ?>
                        <tbody>
                            <tr>
                                <td headers="id"><?php echo $value['id'] ?></td>
                                <td headers="date"><?php echo $value['date'] ?></td>
                                <td headers="or_status_id"><?php
                                    if ($value['or_status_id'] == STATUS_AGUARDANDO_PGT)
                                    {
                                        echo STATUS_AGUARDANDO_PGT_S;
                                    }
                                ?></td>
                                <!--
                                <td headers="details"><a class="btn btn-primary btn-xs" 
                                            href="<?php //echo URL;?>myOrders/details/<?php //echo $value['id'];?>">
                                        Ver Detalhes</a></td>
                                        -->
                            </tr>
                        </tbody>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
