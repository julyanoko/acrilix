$(window).scroll(function () {
    // Retorna a quantidade que o scroll rolou
    var windowsScroll = $(this).scrollTop();

    $('.nav_wrapper').css({
        'transform': 'translate(0px, ' + windowsScroll / 1.36 + '%)'
    });
});