    function mensConfirm(mens, val1, val2, campo, idform, tipo){
        var tipoTela = (tipo =='w' ? BootstrapDialog.TYPE_WARNING : (tipo=='d' ? BootstrapDialog.TYPE_DANGER : (tipo=='s' ? BootstrapDialog.TYPE_SUCCESS : (tipo=='i' ? BootstrapDialog.TYPE_INFO : (tipo=='p' ? BootstrapDialog.TYPE_PRIMARY : BootstrapDialog.TYPE_DEFAULT)  ) ) ) ); 
    	var dialog = new BootstrapDialog({
                          title: '<div align="center">ATEN&Ccedil;&Atilde;O</div>',
                          message: mens,        
                          type: tipoTela,
                          buttons: [{
                                       id: 'btn-1',
                                       label: 'Sim',
                                       cssClass: 'btn-success'
                                     },{
                                       id: 'btn-2',
                                       label: 'N&atilde;o'  
                                   }]
                          });
         dialog.realize();
         var btn1 = dialog.getButton('btn-1');
         var btn2 = dialog.getButton('btn-2');
         btn1.click(function(event){ 
        	document.getElementById(campo).value=val1;
        	document.getElementById(idform).submit();
        });  
        btn2.click(function(event){         	
                    dialog.close();        	
        });
        dialog.open();       
    }

    function modalAdd(mens, tipo, titulo){
        var tipoTela = (tipo =='w' ? BootstrapDialog.TYPE_WARNING : (tipo=='d' ? BootstrapDialog.TYPE_DANGER : (tipo=='s' ? BootstrapDialog.TYPE_SUCCESS : (tipo=='i' ? BootstrapDialog.TYPE_INFO : (tipo=='p' ? BootstrapDialog.TYPE_PRIMARY : BootstrapDialog.TYPE_DEFAULT)  ) ) ) ); 
    	var dialog = new BootstrapDialog({
                          title: '<div align="center">'+titulo+'</div>',
                          message: mens,
                          type: tipoTela,
                          buttons: [{
                                       id: 'btn-1',
                                       label: 'Gravar',
                                       cssClass: 'btn-primary'
                                     },{
                                       id: 'btn-2',
                                       label: 'Cancelar'  
                                   }]
                          });
         dialog.realize();
         var btn1 = dialog.getButton('btn-1');
         var btn2 = dialog.getButton('btn-2');
         btn1.click(function(event){  document.getElementById('formModal').submit();  });  
         btn2.click(function(event){  dialog.close();                                 });
         dialog.open();       
    }
